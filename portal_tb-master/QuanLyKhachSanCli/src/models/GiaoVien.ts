
import { PhanCongGiangDay } from "./PhanCongGiangDay";

export interface GiaoVien {
    GiaoVienID: number;
    TenDayDu: string;
    Ten: string;
    HoVaDem: string;
    MaGiaoVien: string;
    GioiTinh: boolean;
    Email: string;
    SoDienThoai: string;
    KhoPhongQuanLyID?: number;
    NgayVaoTruong?: Date;
    DangLamViec?: boolean;
    UserID: string;
    PhanCongGiangDay: PhanCongGiangDay[];
}
