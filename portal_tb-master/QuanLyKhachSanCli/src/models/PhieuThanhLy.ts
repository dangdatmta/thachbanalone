 
export interface PhieuThanhLy {
    PhieuThanhLyID: number;
    MaPhieu: string;
    ChungTuLienQuan: string;
    NgayLapPhieu: Date;
    GhiChu: string;
    NguoiLapPhieuID: number;
    ChiTietPhieu: string;
    NamHoc: number;
}
