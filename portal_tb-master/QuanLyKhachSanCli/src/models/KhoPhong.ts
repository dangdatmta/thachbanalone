import { ThietBi } from "./ThietBi";
import { GiaoVien } from "./GiaoVien";
import { KhoPhongBoMon } from "./KhoPhongBoMon";

export interface KhoPhong {
    KhoPhongID: number;
    TenKhoPhong: string;
    PhanLoai?: number;
    NamDuaVaoSuDung?: number;
    DiaChi: string;
    DienTich?: number;
    LaPhongChucNang?: boolean; 
    GiaoVien?: GiaoVien;
    ThietBi?: ThietBi[];
    KhoPhongBoMon?: KhoPhongBoMon[];
    MonApDung: string;
    GiaoVienID?: number;
}
