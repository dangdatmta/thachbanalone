 
export interface PhieuBaoHongMat {
    PhieuBaoHongMatID: number;
    MaPhieu: string;
    ChungTuLienQuan: string;
    NgayLapPhieu: Date;
    GhiChu: string;
    NguoiLapPhieuID?: number;
    NguoiLienQuanID?: number;
    TenNguoiLienQuan: string;
    ChiTietPhieu: string;
    NamHoc?: number;
}
