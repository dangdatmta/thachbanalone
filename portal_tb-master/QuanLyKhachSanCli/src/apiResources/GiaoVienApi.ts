import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { GiaoVien } from '@/models/GiaoVien'
export interface GiaoVienApiSearchParams extends Pagination {
    keywords?:number; 
} 
class GiaoVienApi extends BaseApi {
    search(searchParams: GiaoVienApiSearchParams): Promise<PaginatedResponse<GiaoVien>> {

        return new Promise<PaginatedResponse<GiaoVien>>((resolve: any, reject: any) => {
            HTTP.get('giaovien', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<GiaoVien> {
        return new Promise<GiaoVien>((resolve: any, reject: any) => {
            HTTP.get('giaovien/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, giaoVien: GiaoVien): Promise<GiaoVien> {
        return new Promise<GiaoVien>((resolve: any, reject: any) => {
            HTTP.put('giaovien/' + id, 
                giaoVien
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(giaoVien: GiaoVien): Promise<GiaoVien> {
        return new Promise<GiaoVien>((resolve: any, reject: any) => {
            HTTP.post('giaovien', 
                giaoVien
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<GiaoVien> {
        return new Promise<GiaoVien>((resolve: any, reject: any) => {
            HTTP.delete('giaovien/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new GiaoVienApi();
