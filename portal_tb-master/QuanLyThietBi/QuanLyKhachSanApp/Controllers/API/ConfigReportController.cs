﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;
using System.Collections.Generic;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/cauhinh")]
    public class ConfigReportController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var cauhinhbc = await db.ConfigReports.Include(x=>x.BoPhan).ToListAsync();
                if (cauhinhbc == null)
                    return NotFound();
                return Ok(cauhinhbc);
            }
        }

        [HttpGet, Route("{bophanID}")]
        public async Task<IHttpActionResult> GetCRNameByBoPhanID(int bophanID)
        {
            using (var db = new TCEntities())
            {
                var nameConfigReport = await db.ConfigReports.Where(o => o.bophan_id == bophanID).Select(x => new { x.id, x.text, x.link, x.trangthai }).ToListAsync();
                if (nameConfigReport == null)
                    return NotFound();

                return Ok(nameConfigReport);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] ConfigReport cfrp)
        {
            using (var db = new TCEntities())
            {
                if (cfrp.trangthai == null)
                {
                    cfrp.trangthai = 1;
                }
                var idMax = await db.ConfigReports.MaxAsync(x => x.id);
                cfrp.thoigiantao = DateTime.Now;
                cfrp.link = "/auth/Embed/"+ (idMax+1);
                cfrp.trangthai = 1;
                db.ConfigReports.Add(cfrp);
                await db.SaveChangesAsync();

                

            }
            return Ok(cfrp);
        }

        [HttpPut, Route("{cfrpID}")]
        public async Task<IHttpActionResult> Update(int cfrpID, [FromBody] ConfigReport cfrp)
        {
            if (cfrp.id != cfrpID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                cfrp.thoigiansua = DateTime.Now;
                cfrp.BoPhan = null;
                db.Entry(cfrp).State = EntityState.Modified;
                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.ConfigReports.Count(o => o.id == cfrpID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(cfrp);
            }
        }

        [HttpDelete, Route("{cfrpID}")]
        public async Task<String> Delete(int cfrpID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var cfrp = await db.ConfigReports.SingleOrDefaultAsync(o => o.id == cfrpID);
                if (cfrp == null)
                {
                    message = "Không tìm thấy dữ liệu!!!";
                }
                else
                {
                    db.Entry(cfrp).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }

    }
}
