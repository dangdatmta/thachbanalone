﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/embed")]
    public class EmbedController : BaseApiController
    {
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Trust()
        {
            string str;
            var _tableauUrl = "http://192.168.62.113/";
            var _tableauUser = "administrator";
            var _clientIp = "192.168.62.114";
            var site = "";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(_tableauUrl + "trusted");
                var encoding = new UTF8Encoding();
                var postData = "username=" + _tableauUser;
                postData += "&client_ip=" + _clientIp;
                postData += "&target_site=" + site;
                byte[] data = encoding.GetBytes(postData);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                var response = (HttpWebResponse)request.GetResponse();
                str = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return Ok(str);
        }
    }
}
