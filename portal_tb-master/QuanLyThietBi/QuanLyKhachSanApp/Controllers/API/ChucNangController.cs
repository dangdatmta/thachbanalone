﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/chucnang")]
    public class ChucNangController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var chucnang = await db.ChucNangs.ToListAsync();
                if (chucnang == null)
                    return NotFound();
                return Ok(chucnang);
            }
        }

        //lấy ra các id chucnang theo id nhomnsd
        [HttpGet, Route("getchucnangid/{nnsdID}")]
        public async Task<IHttpActionResult> getChucNangIDFollowNhomNSDID(int nnsdID)
        {
            using (var db = new TCEntities())
            {
                var chucnangID = await db.NhomNSD_ChucNang.Where(x => x.nhomnsd_id == nnsdID).Select(x => x.chucnang_id).ToListAsync();
                if (chucnangID == null)
                    return NotFound();
                return Ok(chucnangID);
            }
        }
    }
}