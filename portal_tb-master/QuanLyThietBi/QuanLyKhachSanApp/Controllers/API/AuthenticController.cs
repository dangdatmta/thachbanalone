﻿using HVIT.Security;
using HVITCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Data.Entity;
using QuanLyKhachSanApp.Models;
using System.Threading.Tasks;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        public class LoginForm
        {
            public string tendangnhap { get; set; }
            public string matkhau { get; set; }
            public string RePassword { get; set; }
            public string ChangePasswordKey { get; set; }
            public string OldPassword { get; set; }
        }

        public class UserModel
        {
            public User User { get; set; }
            public List<int> Groups { get; set; }

            public UserModel()
            {
                Groups = new List<int>();
            }
        }

        [HttpPost]
        [Route("login")]
        public async Task<IHttpActionResult> Login(LoginForm login)
        {
            using (TCEntities db = new TCEntities())
            {
                //User user1 = db.Users.Where(x=>x.tendangnhap == login.userName).Include(x => x.User_ChucNang.Select(o=>o.Role.RoleRoleDetails.Select(u=>u.RoleDetail))).FirstOrDefault();
                //.Include(o => o.User_NhomNSDs).Include("User_NhomNSDs.NhomNSD.NhomNSD_ChucNang.ChucNang")
                var id_nhomnsds = await db.User_NhomNSD.Where(x => x.User.tendangnhap == login.tendangnhap).Select(x => x.NhomNSD_id).ToListAsync();
                List<int?> id_chucnangs = new List<int?>();
                var id_chucnang = new List<int?>();
                var id_chucnangsss = new List<int?>();
                foreach (var id_nhomnsd in id_nhomnsds)
                {
                    id_chucnang = await db.NhomNSD_ChucNang.Where(x => x.nhomnsd_id == id_nhomnsd).Select(x => x.chucnang_id).ToListAsync();
                    id_chucnangsss.AddRange(id_chucnang);
                    //id_chucnangs.Add(id_chucnang);
                }
                /*List<string> tenchucnangs = new List<string>();
                foreach (var idchucnangx in id_chucnang)
                {
                    var tenchucnang = await db.ChucNangs.Where(x => x.id == idchucnangx).Select(x => x.ten).FirstOrDefaultAsync();
                    tenchucnangs.Add(tenchucnang);
                }*/
                List<string> chucnangss = new List<string>();
                foreach (var idchucnangx in id_chucnangsss)
                {
                     var chucnangs = await db.ChucNangs.Where(x => x.id == idchucnangx).Select(x => x.ten).FirstOrDefaultAsync();
                     chucnangss.Add(chucnangs);
                }

                User user = await db.Users.SingleOrDefaultAsync(x => x.tendangnhap == login.tendangnhap);
                if (user != null)
                {
                    string passwordInput = AuthenticationHelper.getPassWord(login.matkhau);
                    string passwordUser = user.matkhau;

                    if (passwordInput.Equals(passwordUser))
                    {

                        TokenProvider tokenProvider = new TokenProvider();
                        TokenIdentity token = tokenProvider.GenerateToken(user.id, user.ten,
                            Request.Headers.UserAgent.ToString(),
                            "", DateTime.Now.Ticks);
                        token.SetAuthenticationType("Custom");
                        token.SetIsAuthenticated(true);
                        db.AccessTokenss.Add(new AccessTokens()
                        {
                            Token = token.Token,
                            EffectiveTime = new DateTime(token.EffectiveTime),
                            ExpiresIn = token.ExpiresTime,
                            IP = token.IP,
                            UserAgent = token.UserAgent,
                            UserName = token.Name,
                            UserId = user.id,
                        });
                        await db.SaveChangesAsync();

                        var users = await db.Users
                            .Select(x => new
                            {
                                x.id,
                                x.tendangnhap,
                                x.ten,
                                x.sdt,
                                x.trangthai
                            })
                            .FirstOrDefaultAsync(x => x.id == user.id);
                        return Ok(
                            new
                            {
                                AccessToken = token,
                                Profile = new
                                {
                                    id = users.id,
                                    tendangnhap = users.tendangnhap,
                                    ten = users.ten,
                                    //Roles = user1.User_ChucNang.Select(x => x.ChucNang.ten),
                                    //BoPhans = user1.User_BoPhan.Select(x => x.BoPhan.text),
                                    tenchucnang = chucnangss,
                                    //RoleDetails = user1,
                                    trangthai = users.trangthai,
                                    //countUserRole = user1.User_ChucNang.Count(),
                                    //countUserBoPhan = user1.User_BoPhans.Count(),
                                    //countRoleRoleDetails = user1.UserRoles.Select(x=>x.Role.RoleRoleDetails.Count())
                                    //NhanVien = nhanVien,
                                }
                            }
                        );
                    }
                }
                return Ok("Login failed!");
            }
        }

   /*     [AuthorizeUser, HttpGet]
        [Route("logout")]
        public IHttpActionResult Logout(Guid UserID)
        {
            using (var db = new TCEntities())
            {
                var user = db.Users.Where(x => x.Id == UserID).FirstOrDefault();
                if (user == null)
                    return BadRequest("invalid UserID");
                db.SaveChanges();
                return Ok();
            }
        }*/

        [AuthorizeUser, HttpGet]
        [Route("validate-token")]
        public IHttpActionResult ValidateToken()
        {
            TokenIdentity tokenIdentity = ClaimsPrincipal.Current.Identity as TokenIdentity;
            return Ok();
        }

    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            filterContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}
