﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;
namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/mausac")]
    public class MauSacController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
                var color = await db.Colors.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (color == null)
                    return NotFound();
                return Ok(color);
            }
        }
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] Color color)
        {
            using (var db = new TCEntities())
            {
                var listcolor = await db.Colors.Select(x => x.ten).ToListAsync();
                foreach (var cl in listcolor)
                {
                    if (cl == color.ten)
                    {
                        return BadRequest("Màu sắc này đã tồn tại!!!");
                    }
                }
                db.Colors.Add(color);
                await db.SaveChangesAsync();
            }
            return Ok(color);
        }
        [HttpPut, Route("{colorID}")]
        public async Task<IHttpActionResult> Update(int colorID, [FromBody] Color color)
        {
            if (color.id != colorID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(color).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.Colors.Count(o => o.id == colorID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(color);
            }
        }
        [HttpDelete, Route("{colorID}")]
        public async Task<String> Delete(int colorID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var color = await db.Colors.SingleOrDefaultAsync(o => o.id == colorID);
                var color_permission_bi = await db.PermissionBireport_Color.Where(o => o.color_id == colorID).ToListAsync();
                var color_vbc = await db.NhomViewBC_Color.Where(o => o.color_id == colorID).ToListAsync();
                if (color == null)
                {
                    message = "Không tìm thấy Màu sắc";
                }
                else
                {
                    foreach (var per in color_permission_bi)
                    {
                        db.Entry(per).State = EntityState.Deleted;
                    }
                    foreach (var vbc in color_vbc)
                    {
                        db.Entry(vbc).State = EntityState.Deleted;
                    }
                    db.Entry(color).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}