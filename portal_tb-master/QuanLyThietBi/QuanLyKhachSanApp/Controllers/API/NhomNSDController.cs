﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;
using System.Collections.Generic;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/nhomNSD")]
    public class NhomNSDController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {

                /*var result = await db.NhomNSDs
                .Join(db.User_NhomNSD, d => d.id, m => m.NhomNSD_id,
                    (d, m) => new { nhomNSD_id = d.id, user_id = m.user_id })
                    .GroupBy(g => g.nhomNSD_id)
                    .Select(s => new { s.Key, CountOfUser = s.Count() }).ToListAsync();
                return Ok(result);*/
                var result = await db.NhomNSDs.Include(x=>x.NhomNSD_ChucNang).ToListAsync();
                return Ok(result);
            }
        }

        [HttpGet, Route("{nhomNSDId}")]
        public async Task<IHttpActionResult> Get(int groupUserID)
        {
            using (var db = new TCEntities())
            {
                var role = await db.NhomNSDs
                    .SingleOrDefaultAsync(o => o.id == groupUserID);

                if (role == null)
                    return NotFound();

                return Ok(role);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] NhomNSD groupUser)
        {
            using (var db = new TCEntities())
            {
                if (groupUser.trangthai == null)
                {
                    groupUser.trangthai = 1;
                }
                db.NhomNSDs.Add(groupUser);
                await db.SaveChangesAsync();
            }
            return Ok(groupUser);
        }

        [HttpPut, Route("{groupUserID}")]
        public async Task<IHttpActionResult> Update(int groupUserID, [FromBody] NhomNSD groupUser)
        {
            if (groupUser.id != groupUserID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                var nnsdUpdate = await db.NhomNSDs.Where(x => x.id == groupUserID).FirstOrDefaultAsync();
                var listChucNangIdChecked = groupUser.NhomNSD_ChucNang.Select(x => x.chucnang_id).ToList();//A
                var listChucNangIdOld = await db.NhomNSD_ChucNang.Where(x => x.nhomnsd_id == groupUserID).Select(x => x.chucnang_id).ToListAsync();//B
                List<int?> khongtrungs2 = new List<int?>();//List chua cac phan tu trong B khong trung A
                List<int?> khongtrungs = new List<int?>();//List chua cac phan tu trong A khong trung B
                foreach (var chucnangidchecked in listChucNangIdChecked)
                {
                    if (!listChucNangIdOld.Contains(chucnangidchecked))
                    {
                        khongtrungs.Add(chucnangidchecked);
                    }
                }
                foreach (var chucnangidold in listChucNangIdOld)
                {
                    if (!listChucNangIdChecked.Contains(chucnangidold))
                    {
                        khongtrungs2.Add(chucnangidold);
                    }
                }
                NhomNSD_ChucNang nc = new NhomNSD_ChucNang();
                foreach (var khongtrung in khongtrungs)
                {
                    nc.chucnang_id = khongtrung;
                    nc.nhomnsd_id = groupUserID;
                    db.NhomNSD_ChucNang.Add(nc);
                    await db.SaveChangesAsync();
                }
                foreach (var khongtrung2 in khongtrungs2)
                {
                    var nc2s = await db.NhomNSD_ChucNang.Where(x => x.chucnang_id == khongtrung2 && x.nhomnsd_id == groupUserID).ToListAsync();
                    foreach (var nc2 in nc2s)
                    {
                        db.Entry(nc2).State = EntityState.Deleted;
                    }
                    await db.SaveChangesAsync();
                }

                await db.SaveChangesAsync();
                nnsdUpdate.ten = groupUser.ten;
                nnsdUpdate.trangthai = groupUser.trangthai;
                db.Entry(nnsdUpdate).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.NhomNSDs.Count(o => o.id == groupUserID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(groupUserID);
            }
        }

        [HttpDelete, Route("{nhomNSDId}")]
        public async Task<String> Delete(int nhomNSDId)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var groupUser = await db.NhomNSDs.SingleOrDefaultAsync(o => o.id == nhomNSDId);
                var userGRU = await db.User_NhomNSD.Where(o => o.NhomNSD_id == nhomNSDId).ToListAsync();
                var nnsdChucNang = await db.NhomNSD_ChucNang.Where(o => o.nhomnsd_id == nhomNSDId).ToListAsync();
                if (groupUser == null)
                {
                    message = "Không tìm thấy nhóm ngời dùng !!!";
                }
                else
                {
                    foreach (var ur in userGRU)
                    {
                        db.Entry(ur).State = EntityState.Deleted;
                    }
                    foreach (var nnsdcn in nnsdChucNang)
                    {
                        db.Entry(nnsdcn).State = EntityState.Deleted;
                    }
                    db.Entry(groupUser).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công nhóm người dùng";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }

    }
}
