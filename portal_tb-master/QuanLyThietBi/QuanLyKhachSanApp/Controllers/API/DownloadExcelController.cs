﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Net.Http;
using System.Web;
using QuanLyKhachSanApp.Models;
using System.IO;
using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http.Headers;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/downloadtemplate")]
    public class DownloadExcelController : BaseApiController
    {
        [HttpGet, Route("{nldl_id}")]
        public async Task<HttpResponseMessage> Get(int nldl_id)
        {
            using (var db = new TCEntities())
            {
                var fileName = await db.NhomLoaiDuLieux.Where(o => o.id == nldl_id).Select(x => x.filetemplate).FirstOrDefaultAsync();
                var result = new HttpResponseMessage(HttpStatusCode.OK);

                // 1) Get file bytes
                var filePath = HttpContext.Current.Server
                    .MapPath($"~/FileUpload/{fileName}");

                var fileBytes = File.ReadAllBytes(filePath);

                // 2) Add bytes to a memory stream
                var fileMemStream =
                    new MemoryStream(fileBytes);

                // 3) Add memory stream to response
                result.Content = new StreamContent(fileMemStream);

                // 4) build response headers
                var headers = result.Content.Headers;

                headers.ContentDisposition =
                    new ContentDispositionHeaderValue("attachment");
                headers.ContentDisposition.FileName = fileName;
                
                headers.ContentType =
                    //new MediaTypeHeaderValue("application/xlsx");
                    new MediaTypeHeaderValue("application/octet-stream");

                headers.ContentLength = fileMemStream.Length;
                return result;
            } 
        }

        [HttpGet, Route("getname/{nldl_id}")]
        public async Task<IHttpActionResult> GetNameofFile(int nldl_id)
        {
            using (var db = new TCEntities())
            {
                var fileName = await db.NhomLoaiDuLieux.Where(o => o.id == nldl_id).Select(x => x.filetemplate).FirstOrDefaultAsync();
                //var bp = await db.BoPhans.OrderBy(x=>x.stt).ToListAsync();
                if (fileName == null)
                    return NotFound();
                return Ok(fileName);
            }
        }
    }
}
