﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Net.Http;
using System.Web;
using QuanLyKhachSanApp.Models;
using System.IO;
using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.Net;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/kd01cs")]
    public class ImportKD01CSController : BaseApiController
    {
        [HttpGet, Route("{nldl_id},{kybc_id}")]
        public async Task<IHttpActionResult> Get(int nldl_id, int kybc_id)
        {
            using (var db = new TCEntities())
            {
                List<int?> stt = await db.KD01CS.Where(m => m.NLDL_KyBC.NLDL_id == nldl_id && m.NLDL_KyBC.kybc_id == kybc_id).Select(o => o.stt).Distinct().OrderByDescending(x => x.Value).ToListAsync();
                if (stt == null)
                    return NotFound();
                return Ok(stt);
            }
        }
        [HttpGet, Route("{stt},{nldl_id},{kybc_id}")]
        public async Task<IHttpActionResult> GetByStt(int stt, int nldl_id, int kybc_id)
        {
            using (var db = new TCEntities())
            {
                //var bc = await db.KD01CS.Where(o => o.stt == stt).ToListAsync();
                var bc = await db.KD01CS.Where(o => o.NLDL_KyBC.NLDL_id == nldl_id && o.NLDL_KyBC.kybc_id == kybc_id).Where(o => o.stt == stt).ToListAsync();
                if (bc == null)
                    return NotFound();

                return Ok(bc);
            }
        }
        [HttpPost, Route("{nldl_id},{kybc_id}")]
        public string ExcelUpload(int nldl_id, int kybc_id)
        {
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (var objEntity = new TCEntities())
            {
                string nguoitao = String.Join("", HttpContext.Current.Request.Params["nguoitao"].Split('\"', '\"'));

                if (httpRequest.Files.Count > 0)
                {
                    HttpPostedFile file = httpRequest.Files[0];
                    Stream stream = file.InputStream;

                    IExcelDataReader reader = null;

                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();
                        
                        var verMax = objEntity.KD01CS.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.version);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        int? stt = objEntity.KD01CS.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.stt);
                        if (stt == null)
                        {
                            stt = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            KD01CS objUser = new KD01CS();
                            objUser.header1 = finalRecords.Rows[0][0].ToString();
                            objUser.header2 = finalRecords.Rows[0][1].ToString();
                            objUser.header3 = finalRecords.Rows[0][2].ToString();
                            objUser.header4 = finalRecords.Rows[0][3].ToString();
                            objUser.header5 = finalRecords.Rows[0][4].ToString();
                            objUser.header6 = finalRecords.Rows[0][5].ToString();
                            objUser.header7 = finalRecords.Rows[0][6].ToString();
                            objUser.header8 = finalRecords.Rows[0][7].ToString();
                            objUser.header9 = finalRecords.Rows[0][8].ToString();
                            objUser.loaigach = finalRecords.Rows[i][0].ToString();
                            objUser.phamcap = finalRecords.Rows[i][1].ToString();
                            objUser.kichthuoc = finalRecords.Rows[i][2].ToString();
                            objUser.nhanh = finalRecords.Rows[i][3].ToString();
                            objUser.kenh = finalRecords.Rows[i][4].ToString();
                            objUser.sanluongkehoach = decimal.Parse(finalRecords.Rows[i][5].ToString());
                            objUser.doanhsokehoach = decimal.Parse(finalRecords.Rows[i][6].ToString());
                            objUser.doanhthukehoach = decimal.Parse(finalRecords.Rows[i][7].ToString());
                            objUser.giathanhkehoach = decimal.Parse(finalRecords.Rows[i][8].ToString());
                            objUser.ngaytao = DateTime.Now;
                            objUser.version = verMax + 1;
                            objUser.stt = stt + 1;
                            objUser.nguoitao = nguoitao;
                            objUser.nldl_kybc_id = objEntity.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id && x.kybc_id == kybc_id).Select(o => o.id).FirstOrDefault();
                            objEntity.KD01CS.Add(objUser);
                        }
                        int output = objEntity.SaveChanges();
                        if (output > 0)
                        {
                            message = "Excel file has been successfully uploaded";
                        }
                        else
                        {
                            message = "Excel file uploaded has faild";
                        }
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();

                        var verMax = objEntity.KD01CS.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.version);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        int? stt = objEntity.KD01CS.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.stt);
                        if (stt == null)
                        {
                            stt = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            KD01CS objUser = new KD01CS();
                            objUser.header1 = finalRecords.Rows[0][0].ToString();
                            objUser.header2 = finalRecords.Rows[0][1].ToString();
                            objUser.header3 = finalRecords.Rows[0][2].ToString();
                            objUser.header4 = finalRecords.Rows[0][3].ToString();
                            objUser.header5 = finalRecords.Rows[0][4].ToString();
                            objUser.header6 = finalRecords.Rows[0][5].ToString();
                            objUser.header7 = finalRecords.Rows[0][6].ToString();
                            objUser.header8 = finalRecords.Rows[0][7].ToString();
                            objUser.header9 = finalRecords.Rows[0][8].ToString();
                            objUser.loaigach = finalRecords.Rows[i][0].ToString();
                            objUser.phamcap = finalRecords.Rows[i][1].ToString();
                            objUser.kichthuoc = finalRecords.Rows[i][2].ToString();
                            objUser.nhanh = finalRecords.Rows[i][3].ToString();
                            objUser.kenh = finalRecords.Rows[i][4].ToString();
                            objUser.sanluongkehoach = decimal.Parse(finalRecords.Rows[i][5].ToString());
                            objUser.doanhsokehoach = decimal.Parse(finalRecords.Rows[i][6].ToString());
                            objUser.doanhthukehoach = decimal.Parse(finalRecords.Rows[i][7].ToString());
                            objUser.giathanhkehoach = decimal.Parse(finalRecords.Rows[i][8].ToString());
                            objUser.ngaytao = DateTime.Now;
                            objUser.version = verMax + 1;
                            objUser.stt = stt + 1;
                            objUser.nguoitao = nguoitao;
                            objUser.nldl_kybc_id = objEntity.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id && x.kybc_id == kybc_id).Select(o => o.id).FirstOrDefault();
                            objEntity.KD01CS.Add(objUser);
                        }
                        int output = objEntity.SaveChanges();
                        if (output > 0)
                        {
                            message = "Excel file has been successfully uploaded";
                        }
                        else
                        {
                            message = "Excel file uploaded has faild";
                        }
                    }
                    else
                    {
                        message = "This file format is not supported";
                    }

                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            return message;
        }
        [HttpPost, Route("{version}")]
        public async Task<IHttpActionResult> ExcelOverride(int version)
        {
            using (var db = new TCEntities())
            {
                List<KD01CS> listReportFollowVer = await db.KD01CS.Where(x => x.stt == version).ToListAsync();
                KD01CS_TRASH f_tckt01_trash = new KD01CS_TRASH();
                var sttMax = db.KD01CS.Max(x => x.stt);
                List<KD01CS> listReportFollowVer1 = await db.KD01CS.Where(x => x.stt == sttMax).ToListAsync();
                foreach (var reportFollowVer in listReportFollowVer1)
                {
                    reportFollowVer.stt = version;
                    db.Entry(reportFollowVer).State = EntityState.Modified;
                    db.SaveChanges();
                }

                foreach (var reportFollowVer in listReportFollowVer)
                {
                    f_tckt01_trash.doanhsokehoach = reportFollowVer.doanhsokehoach;
                    f_tckt01_trash.doanhthukehoach = reportFollowVer.doanhthukehoach;
                    f_tckt01_trash.giathanhkehoach = reportFollowVer.giathanhkehoach;
                    f_tckt01_trash.kenh = reportFollowVer.kenh;
                    f_tckt01_trash.kichthuoc = reportFollowVer.kichthuoc;
                    f_tckt01_trash.loaigach = reportFollowVer.loaigach;
                    f_tckt01_trash.nhanh = reportFollowVer.nhanh;
                    f_tckt01_trash.phamcap = reportFollowVer.phamcap;
                    f_tckt01_trash.sanluongkehoach = reportFollowVer.sanluongkehoach;
                    f_tckt01_trash.version = reportFollowVer.version;

                    db.KD01CS_TRASH.Add(f_tckt01_trash);
                    reportFollowVer.stt = sttMax;
                    db.Entry(reportFollowVer).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Ok(f_tckt01_trash);
            }
        }


        [HttpPost, Route("validate")]
        public List<string> ValidateExcel()
        {
            List<string> listmessage = new List<string>();
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (var objEntity = new TCEntities())
            {
                if (httpRequest.Files.Count > 0)
                {
                    HttpPostedFile file = httpRequest.Files[0];
                    Stream stream = file.InputStream;

                    IExcelDataReader reader = null;

                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();
                        int? verMax = objEntity.ValidateKD01CSs.Max(x => x.ver);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            if (finalRecords.Rows[i][0].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 1 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][1].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 2 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][2].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 3 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][3].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 4 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][4].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 5 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][5].GetType() != typeof(float))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 6 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][6].GetType() != typeof(float))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 7 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][7].GetType() != typeof(float))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 8 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][8].GetType() != typeof(float))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 9 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            ValidateKD01CS objUser = new ValidateKD01CS();
                            objUser.loaigach = finalRecords.Rows[i][0].ToString();
                            objUser.phamcap = finalRecords.Rows[i][1].ToString();
                            objUser.kichthuoc = finalRecords.Rows[i][2].ToString();
                            objUser.nhanh = finalRecords.Rows[i][3].ToString();
                            objUser.kenh = finalRecords.Rows[i][4].ToString();
                            objUser.sanluongkehoach = finalRecords.Rows[i][5].ToString();
                            objUser.doanhsokehoach = finalRecords.Rows[i][6].ToString();
                            objUser.doanhthukehoach = finalRecords.Rows[i][7].ToString();
                            objUser.giathanhkehoach = finalRecords.Rows[i][8].ToString();
                            objUser.ver = verMax + 1;
                            objEntity.ValidateKD01CSs.Add(objUser);
                            objEntity.SaveChanges();
                        }
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();
                        var finalRecords = excelRecords.Tables[0];
                        int? verMax = objEntity.ValidateKD01CSs.Max(x => x.ver);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            if (finalRecords.Rows[i][0].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 1 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][1].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 2 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][2].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 3 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][3].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 4 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][4].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 5 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][5].GetType() != typeof(double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 6 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][6].GetType() != typeof(double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 7 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][7].GetType() != typeof(double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 8 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][8].GetType() != typeof(double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 9 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }
                            ValidateKD01CS objUser = new ValidateKD01CS();
                            objUser.loaigach = finalRecords.Rows[i][0].ToString();
                            objUser.phamcap = finalRecords.Rows[i][1].ToString();
                            objUser.kichthuoc = finalRecords.Rows[i][2].ToString();
                            objUser.nhanh = finalRecords.Rows[i][3].ToString();
                            objUser.kenh = finalRecords.Rows[i][4].ToString();
                            objUser.sanluongkehoach = finalRecords.Rows[i][5].ToString();
                            objUser.doanhsokehoach = finalRecords.Rows[i][6].ToString();
                            objUser.doanhthukehoach = finalRecords.Rows[i][7].ToString();
                            objUser.giathanhkehoach = finalRecords.Rows[i][8].ToString();
                            objUser.ver = verMax + 1;
                            objEntity.ValidateKD01CSs.Add(objUser);
                            objEntity.SaveChanges();
                        }
                    }
                    else
                    {
                        message = "Định dạng file không được hỗ trợ!!!";
                        listmessage.Add(message);
                    }
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            return listmessage;
        }

        [HttpGet, Route("validate")]
        public async Task<IHttpActionResult> GetValidate()
        {
            using (var db = new TCEntities())
            {
                //List<F_CSKH_05> bc = new List<F_CSKH_05>();
                /*List<int?> ver = await (from bc in db.F_CSKH_05
                                        select bc.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();*/

                //List<int?> ver = await db.F_CSKH_05.Select(o=>o.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();
                int? verMax = db.ValidateKD01CSs.Max(x => x.ver);
                var tckt01Validate = await db.ValidateKD01CSs.Where(x => x.ver == verMax).ToListAsync();

                //var ver = await db.F_CSKH_05.ToListAsync();
                //List<int?> ver = await db.F_CSKH_05.Select(o => o.version).OrderByDescending().Distinct().ToListAsync();
                //bc = await db.F_CSKH_05.GroupBy(x=>x.version).Select(g=>g.FirstOrDefault()).ToListAsync();

                if (tckt01Validate == null)
                    return NotFound();

                return Ok(tckt01Validate);
            }
        }
    }
}
