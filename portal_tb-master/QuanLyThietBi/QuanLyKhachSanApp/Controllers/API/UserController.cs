﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var user = await db.Users.Include(x => x.User_BoPhans).Include(x => x.User_NhomNSDs)
                                         .Include("User_BoPhans.BoPhan").Include("User_NhomNSDs.NhomNSD").ToListAsync();
                //.Include("User_NhomNSD.NhomNSD").Include("NhomNSD.NhomNSD_ChucNang").Include("NhomNSD_ChucNang.ChucNang")
                //var user = await db.Users.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [HttpGet, Route("{userId}")]
        public async Task<IHttpActionResult> GetById(int userId)
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").SingleOrDefaultAsync(o => o.Id == userId);
                var user = await db.Users.SingleOrDefaultAsync(o => o.id == userId);
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }
        //lấy ra các id bộ phận theo id user
        [HttpGet, Route("getbophanid/{userId}")]
        public async Task<IHttpActionResult> GetBoPhanIDFollowUserID(int userId)
        {
            using (var db = new TCEntities())
            {
                var bpID = await db.User_BoPhan.Where(x => x.User_id == userId).Select(x => x.BoPhan_id).ToListAsync();
                if (bpID == null)
                    return NotFound();
                return Ok(bpID);
            }
        }
        //lấy ra các id nhom nsd theo id user
        [HttpGet, Route("getnnsdid/{userId}")]
        public async Task<IHttpActionResult> GetNNSDIDFollowUserID(int userId)
        {
            using (var db = new TCEntities())
            {
                var nnsdID = await db.User_NhomNSD.Where(x => x.user_id == userId).Select(x => x.NhomNSD_id).ToListAsync();
                if (nnsdID == null)
                    return NotFound();
                return Ok(nnsdID);
            }
        }
        [HttpPost, Route("")]
        public async Task<string> Insert([FromBody] User user)
        {
            string mess = "";
            using (var db = new TCEntities())
            {
                List<string> listUserName = await db.Users.Select(x => x.tendangnhap).ToListAsync();
                foreach (var userName in listUserName)
                {
                    if (user.tendangnhap == userName)
                    {
                        mess = "UserName này đã tồn tại!!!";
                    }
                }
                if (mess == "UserName này đã tồn tại!!!")
                {
                    return mess;
                }
                else
                {
                    if (user.trangthai == null)
                    {
                        user.trangthai = 1;
                    }
                    db.Users.Add(user);
                    await db.SaveChangesAsync();
                    return mess;
                }
            }
            
        }


        //public async Task<IHttpActionResult> Update(int userID, [FromBody] User user)
        //{
        //    if (user.id != userID) return BadRequest("Id mismatch");

        //    /*if (!ModelState.IsValid)
        //      {
        //          return BadRequest(ModelState);
        //      }*/
        //    using (var db = new TCEntities())
        //    {

        //        var userCurrent = await db.Users.SingleOrDefaultAsync(o => o.id == userID);
        //        //var userRoles = await db.UserRoles.Where(o => o.UserId == userID).ToListAsync();
        //        //var userBSN = await db.UserBussinessGroups.Where(o => o.UserId == userID).ToListAsync();
        //        //var userGrU = await db.UserGroupUsers.Where(o => o.UserId == userID).ToListAsync();
        //        //foreach (var UGR in userGrU)
        //        //{
        //        //    db.Entry(UGR).State = EntityState.Deleted;
        //        //}
        //        //foreach (var UBG in userBSN)
        //        //{
        //        //    db.Entry(UBG).State = EntityState.Deleted;
        //        //}
        //        //foreach (var UR in userRoles)
        //        //{
        //        //    db.Entry(UR).State = EntityState.Deleted;
        //        //}
        //        userCurrent.ten = user.ten;
        //        userCurrent.tendangnhap = user.tendangnhap;
        //        userCurrent.matkhau = user.matkhau;
        //        userCurrent.nguoisua = user.nguoisua;
        //        //db.Entry(userCurrent).State = EntityState.Deleted;
        //        //await db.SaveChangesAsync();

        //        //db.Users.Add(user);

        //        try
        //        {
        //            await db.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException ex)
        //        {
        //            bool exists = db.Users.Count(o => o.id == userID) > 0;
        //            if (!exists)
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw ex;
        //            }
        //        }
        //        return Ok(user);
        //    }
        //}
        [HttpPut, Route("{userId}")]
        public async Task<IHttpActionResult> Update(int userID, [FromBody] User user)
        {
            if (user.id != userID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                var userUpdate = await db.Users.Where(x => x.id == userID).FirstOrDefaultAsync();
                var listBoPhanIdChecked = user.User_BoPhans.Select(x => x.BoPhan_id).ToList();//A
                var listBoPhanIdOld = await db.User_BoPhan.Where(x => x.User_id == userID).Select(x => x.BoPhan_id).ToListAsync();//B
                var listnnsdIdChecked = user.User_NhomNSDs.Select(x => x.NhomNSD_id).ToList();//C
                var listnnsdIdOld = await db.User_NhomNSD.Where(x => x.user_id == userID).Select(x => x.NhomNSD_id).ToListAsync();//D
                List<int?> khongtrungs2 = new List<int?>();//List chua cac phan tu trong B khong trung A
                List<int?> khongtrungs = new List<int?>();//List chua cac phan tu trong A khong trung B
                List<int?> khongtrungnnsds2 = new List<int?>();//List chua cac phan tu trong C khong trung D
                List<int?> khongtrungnnsds = new List<int?>();//List chua cac phan tu trong D khong trung C
                foreach (var bophanidchecked in listBoPhanIdChecked)
                {
                    if (!listBoPhanIdOld.Contains(bophanidchecked))
                    {
                        khongtrungs.Add(bophanidchecked);
                    }
                }
                foreach (var bophanidold in listBoPhanIdOld)
                {
                    if (!listBoPhanIdChecked.Contains(bophanidold))
                    {
                        khongtrungs2.Add(bophanidold);
                    }
                }
                User_BoPhan ub = new User_BoPhan();
                foreach (var khongtrung in khongtrungs)
                {
                    ub.BoPhan_id = khongtrung;
                    ub.User_id = userID;
                    db.User_BoPhan.Add(ub);
                    await db.SaveChangesAsync();
                }
                foreach (var khongtrung2 in khongtrungs2)
                {
                    var ub2s = await db.User_BoPhan.Where(x => x.BoPhan_id == khongtrung2 && x.User_id == userID).ToListAsync();
                    foreach ( var ub2 in ub2s)
                    {
                        db.Entry(ub2).State = EntityState.Deleted;
                    }
                    await db.SaveChangesAsync();
                }



                foreach (var nnsdIdChecked in listnnsdIdChecked)
                {
                    if (!listnnsdIdOld.Contains(nnsdIdChecked))
                    {
                        khongtrungnnsds.Add(nnsdIdChecked);
                    }
                }
                foreach (var nnsdIdOld in listnnsdIdOld)
                {
                    if (!listnnsdIdChecked.Contains(nnsdIdOld))
                    {
                        khongtrungnnsds2.Add(nnsdIdOld);
                    }
                }
                User_NhomNSD un = new User_NhomNSD();
                foreach (var khongtrungnnsd in khongtrungnnsds)
                {
                    un.NhomNSD_id = khongtrungnnsd;
                    un.user_id = userID;
                    db.User_NhomNSD.Add(un);
                    await db.SaveChangesAsync();
                }
                foreach (var khongtrungnnsd2 in khongtrungnnsds2)
                {
                    var un2s = await db.User_NhomNSD.Where(x => x.NhomNSD_id == khongtrungnnsd2 && x.user_id == userID).ToListAsync();
                    foreach (var un2 in un2s)
                    {
                        db.Entry(un2).State = EntityState.Deleted;
                    }
                    await db.SaveChangesAsync();
                }

                await db.SaveChangesAsync();
                userUpdate.tenbophan = user.tenbophan;
                userUpdate.trangthai = user.trangthai;
                userUpdate.ten = user.ten;
                userUpdate.tendangnhap = user.tendangnhap;
                userUpdate.email = user.email;
                userUpdate.matkhau = user.matkhau;

                db.Entry(userUpdate).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.Users.Count(o => o.id == userID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(user);
            }
        }

        [HttpDelete, Route("{userId}")]
        public async Task<String> Delete(int userID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var user = await db.Users.SingleOrDefaultAsync(o => o.id == userID);
                var userBSN = await db.User_BoPhan.Where(o => o.User_id == userID).ToListAsync();
                var userGrU = await db.User_NhomNSD.Where(o => o.user_id == userID).ToListAsync();
                var userAccessToken = await db.AccessTokenss.Where(o => o.UserId == userID).ToListAsync();
                if (user == null)
                {
                    message = "Không tìm thấy User";
                }
                else
                {
                    foreach (var UAT in userAccessToken)
                    {
                        db.Entry(UAT).State = EntityState.Deleted;
                    }
                    foreach (var UGR in userGrU)
                    {
                        db.Entry(UGR).State = EntityState.Deleted;
                    }
                    foreach (var UBG in userBSN)
                    {
                        db.Entry(UBG).State = EntityState.Deleted;
                    }
                    db.Entry(user).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa người dùng thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}