namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TinhThanh")]
    public partial class TinhThanh
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TinhThanh()
        {
            NhomViewBC_TinhThanh = new HashSet<NhomViewBC_TinhThanh>();
            PermissionBireport_TinhThanh = new HashSet<PermissionBireport_TinhThanh>();
            DaiLys = new HashSet<DaiLy>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [StringLength(5)]
        public string code { get; set; }
        [StringLength(100)]
        public string ten { get; set; }

        public int? khuvuc_id { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        public KhuVuc Khuvuc { get; set; }
        public ICollection<NhomViewBC_TinhThanh> NhomViewBC_TinhThanh { get; set; }
        public ICollection<PermissionBireport_TinhThanh> PermissionBireport_TinhThanh { get; set; }
        public ICollection<DaiLy> DaiLys { get; set; }
    }
}
