namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            User_BoPhans = new HashSet<User_BoPhan>();
            User_NhomNSDs = new HashSet<User_NhomNSD>();
            AccessTokenss = new HashSet<AccessTokens>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(75)]
        public string ten { get; set; }

        [StringLength(20)]
        public string sdt { get; set; }
        [StringLength(100)]
        public string bophan { get; set; }
        [StringLength(50)]
        public string email { get; set; }

        [StringLength(50)]
        public string tendangnhap { get; set; }

        public int? trangthai { get; set; }

        public DateTime? dangnhaplancuoi { get; set; }

        [StringLength(100)]
        public string tenbophan { get; set; }

        public int? nhomviewbc_id { get; set; }

        public int? permission_bireport_id { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        [StringLength(50)]
        public string matkhau { get; set; }
        public ICollection<User_BoPhan> User_BoPhans { get; set; }
        public ICollection<User_NhomNSD> User_NhomNSDs { get; set; }
        public ICollection<AccessTokens> AccessTokenss { get; set; }
    }
}
