namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SanPham")]
    public partial class SanPham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SanPham()
        {
            NhomViewBC_SanPham = new HashSet<NhomViewBC_SanPham>();
            PermissionBireport_SanPham = new HashSet<PermissionBireport_SanPham>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(150)]
        public string ten { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public ICollection<NhomViewBC_SanPham> NhomViewBC_SanPham { get; set; }
        public ICollection<PermissionBireport_SanPham> PermissionBireport_SanPham { get; set; }
    }
}
