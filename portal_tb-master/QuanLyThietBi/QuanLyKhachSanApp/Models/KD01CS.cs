﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[KD01CS]")]
    public partial class KD01CS
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string loaigach { get; set; }
        [StringLength(100)]
        public string phamcap { get; set; }
        [StringLength(100)]
        public string kichthuoc { get; set; }
        [StringLength(100)]
        public string nhanh { get; set; }
        [StringLength(100)]
        public string kenh { get; set; }
        public decimal sanluongkehoach { get; set; }
        public decimal doanhsokehoach { get; set; }
        public decimal doanhthukehoach { get; set; }
        public decimal giathanhkehoach { get; set; }
        public DateTime? ngaytao { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        public int? nldl_kybc_id { get; set; }

        public int? version { get; set; }

        public int? stt { get; set; }
        [StringLength(100)]
        public string header1 { get; set; }
        [StringLength(100)]
        public string header2 { get; set; }
        [StringLength(100)]
        public string header3 { get; set; }
        [StringLength(100)]
        public string header4 { get; set; }
        [StringLength(100)]
        public string header5 { get; set; }
        [StringLength(100)]
        public string header6 { get; set; }
        [StringLength(100)]
        public string header7 { get; set; }
        [StringLength(100)]
        public string header8 { get; set; }
        [StringLength(100)]
        public string header9 { get; set; }
        public NLDL_KyBC NLDL_KyBC { get; set; }

    }
}
