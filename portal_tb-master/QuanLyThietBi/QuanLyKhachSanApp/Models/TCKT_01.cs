namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[TCKT.01]")]
    public partial class TCKT_01
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string model { get; set; }

        public double? giatamtinh { get; set; }

        public DateTime? ngaytao { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        public int? nldl_kybc_id { get; set; }

        public int? version { get; set; }

        public int? stt { get; set; }
        [StringLength(100)]
        public string header1 { get; set; }
        [StringLength(100)]
        public string header2 { get; set; }
        public NLDL_KyBC NLDL_KyBC { get; set; }

    }
}
