﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[KD01CS_TRASH]")]
    public partial class KD01CS_TRASH
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string loaigach { get; set; }
        [StringLength(100)]
        public string phamcap { get; set; }
        [StringLength(100)]
        public string kichthuoc { get; set; }
        [StringLength(100)]
        public string nhanh { get; set; }
        [StringLength(100)]
        public string kenh { get; set; }
        public decimal sanluongkehoach { get; set; }
        public decimal doanhsokehoach { get; set; }
        public decimal doanhthukehoach { get; set; }
        public decimal giathanhkehoach { get; set; }
        public int? version { get; set; }
    }
}
