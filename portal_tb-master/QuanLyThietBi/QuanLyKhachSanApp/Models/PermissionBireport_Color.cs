namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PermissionBireport_Color
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? permission_bireport_id { get; set; }

        public int? color_id { get; set; }

        public Color Color { get; set; }

        public Permission_bireport Permission_bireport { get; set; }
    }
}
