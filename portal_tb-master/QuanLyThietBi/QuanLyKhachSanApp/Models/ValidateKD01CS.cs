﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[Validate_KD01CS]")]
    public partial class ValidateKD01CS
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public String loaigach { get; set; }
        public String phamcap { get; set; }
        public String kichthuoc { get; set; }
        public String nhanh { get; set; }
        public String kenh { get; set; }
        public String sanluongkehoach { get; set; }
        public String doanhsokehoach { get; set; }
        public String doanhthukehoach { get; set; }
        public String giathanhkehoach { get; set; }
        public int? ver { get; set; }
    }
}
