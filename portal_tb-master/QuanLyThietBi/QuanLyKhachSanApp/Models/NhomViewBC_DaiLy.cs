namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NhomViewBC_DaiLy
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? nhomviewbc_id { get; set; }

        public int? daily_id { get; set; }

        public DaiLy DaiLy { get; set; }

        public NhomViewBC NhomViewBC { get; set; }
    }
}
