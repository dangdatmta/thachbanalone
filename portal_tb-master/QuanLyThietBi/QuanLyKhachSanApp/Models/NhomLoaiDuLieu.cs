namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NhomLoaiDuLieu")]
    public partial class NhomLoaiDuLieu
    {
        public NhomLoaiDuLieu()
        {
            NLDL_KyBCs = new HashSet<NLDL_KyBC>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(50)]
        public string code { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [StringLength(500)]
        public string filetemplate { get; set; }

        public int? loaiky { get; set; }

        [StringLength(100)]
        public string tenbang_sql { get; set; }

        public int? bophan_id { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public BoPhan BoPhan { get; set; }
        public ICollection<NLDL_KyBC> NLDL_KyBCs { get; set; }
    }
}
