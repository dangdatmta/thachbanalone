namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PermissionBireport_SanPham
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? permission_bireport_id { get; set; }

        public int? sanpham_id { get; set; }

        public Permission_bireport Permission_bireport { get; set; }

        public SanPham SanPham { get; set; }
    }
}
