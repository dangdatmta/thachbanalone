namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NhomNSD")]
    public partial class NhomNSD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NhomNSD()
        {
            User_NhomNSD = new HashSet<User_NhomNSD>();
            NhomNSD_ChucNang = new HashSet<NhomNSD_ChucNang>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(150)]
        public string ten { get; set; }

        public int? trangthai { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public ICollection<User_NhomNSD> User_NhomNSD { get; set; }
        public ICollection<NhomNSD_ChucNang> NhomNSD_ChucNang { get; set; }
    }
}
