﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NhomNSD_ChucNang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? nhomnsd_id { get; set; }

        public int? chucnang_id { get; set; }

        public ChucNang ChucNang { get; set; }

        public NhomNSD NhomNSD { get; set; }
    }
}
