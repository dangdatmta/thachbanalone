namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KhuVuc")]
    public partial class KhuVuc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KhuVuc()
        {
            NhomViewBC_KhuVuc = new HashSet<NhomViewBC_KhuVuc>();
            PermissionBireport_KhuVuc = new HashSet<PermissionBireport_KhuVuc>();
            TinhThanhs = new HashSet<TinhThanh>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string ten { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        [StringLength(50)]
        public string code { get; set; }
        public int? trangthai { get; set; }
        public ICollection<TinhThanh> TinhThanhs { get; set; }
        public ICollection<NhomViewBC_KhuVuc> NhomViewBC_KhuVuc { get; set; }
        public ICollection<PermissionBireport_KhuVuc> PermissionBireport_KhuVuc { get; set; }
    }
}
