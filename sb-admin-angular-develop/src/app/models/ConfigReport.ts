export class ConfigReport {
    id?: number
    trangthai?: number
    thutu?: number
    bophan_id?: number
    text?: string
    link?: string
    tableurl_id?: string
    user_id?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date

}