import { NhomLoaiDuLieu } from "./NhomLoaiDuLieu"

export class KyBaoCao {
    id?: number
    nam?: number
    quy?: number
    thang?: number
    ngaybatdau?: Date
    ngayketthuc?: Date
    ghichu?: string
    trangthai?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    ten?: string
    loaiky?: number
    nhomloaidulieus?: NhomLoaiDuLieu[]
}