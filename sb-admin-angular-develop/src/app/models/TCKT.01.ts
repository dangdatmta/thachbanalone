import { NLDL_KyBC } from "./NLDL_KyBC"

export class TCKT01 {
    id?: number
    header1?: string
    header2?: string
    model?: string
    giatamtinh?: Float32Array
    ngaytao?: Date
    nguoitao?: string
    nhomloaidulieu_id?: number
    kybaocao_id?: number
    version?: number
    stt?: number
    ndl_kybc_id?: number
    nldl_kybc?: NLDL_KyBC
    
}