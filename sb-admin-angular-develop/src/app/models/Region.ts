import { TinhThanh } from "./TinhThanh"
import { NhomViewBC_KhuVuc } from "./NhomViewBC_KhuVuc"
export class Region {
    id?: number
    ten?: string
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    code?: string
    trangthai?: number
    TinhThanhs?: TinhThanh[]
    NhomViewBC_KhuVucs?: NhomViewBC_KhuVuc[]
}