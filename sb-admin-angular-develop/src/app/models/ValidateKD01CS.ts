export class ValidateKD01CS {
    loaigach?: number;
    phamcap?: number;
    kichthuoc?: number;
    nhanh?: number;
    kenh?: number;
    sanluongkehoach?: Float64Array;
    doanhsokehoach?: Float64Array;
    doanhthukehoach?: Float64Array;
    giathanhkehoach?: Float64Array;
    ver?: number;
    id?: number;
}