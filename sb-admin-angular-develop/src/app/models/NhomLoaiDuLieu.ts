import { BoPhan } from "./BoPhan"
import { NLDL_KyBC } from "./NLDL_KyBC"

export class NhomLoaiDuLieu {
    id?:number
    code?:string
    name?:string
    filetemplate?:File
    loaiky?:string
    tenbang_sql?:string
    bophan_id?:number
    nguoitao?:string
    nguoisua?:string
    thoigiantao?:Date
    thoigiansua?:Date
    BoPhan?: BoPhan
    NLDL_KyBCs?: NLDL_KyBC[]
}