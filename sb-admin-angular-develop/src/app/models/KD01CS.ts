import { NLDL_KyBC } from "./NLDL_KyBC";

export class KD01CS {
    id?: number;
    loaigach?: string;
    phamcap?: string;
    kichthuoc?: string;
    nhanh?: string;
    kenh?: string;
    sanluongkehoach?: Float64Array;
    doanhsokehoach?: Float64Array;
    doanhthukehoach?: Float64Array;
    giathanhkehoach?: Float64Array;
    stt?: number;
    version?: number;
    header1?: string;
    header2?: string;
    header3?: string;
    header4?: string;
    header5?: string;
    header6?: string;
    header7?: string;
    header8?: string;
    header9?: string;
    nldl_kybc_id?: number;
    ngaytao?: Date;
    nguoitao?: string;
    nldl_kybc?: NLDL_KyBC;
}