import { NhomViewBC_KhuVuc } from "./NhomViewBC_KhuVuc"

export class NhomViewBC {
    id?: number
    ten?: string
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    NhomViewBC_KhuVucs?: NhomViewBC_KhuVuc[]
}