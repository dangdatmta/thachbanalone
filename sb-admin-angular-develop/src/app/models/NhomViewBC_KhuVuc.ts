import { Region } from "./Region"
import { NhomViewBC } from "./NhomViewBC"

export class NhomViewBC_KhuVuc {
    id?:number
    nhomviewbc_id?:number
    khuvuc_id?:number
    KhuVucs?: Region
    NhomViewBCs?: NhomViewBC
}