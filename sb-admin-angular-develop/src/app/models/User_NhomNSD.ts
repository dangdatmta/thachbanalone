import { NhomNSD } from "./NhomNSD"
import { User } from "./User"

export class User_NhomNSD {
    constructor(ndsd_id: number){
        this.NhomNSD_id = ndsd_id;
    }
    id?: number
    user_id?: number
    NhomNSD_id?: number
    User?: User
    NhomNSD?: NhomNSD
}