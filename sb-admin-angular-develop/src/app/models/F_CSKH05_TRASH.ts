import { User } from "./User";

export class F_CSKH05_TRASH{
    Id?: string;  
    CompanyCode?: string;  
    MAKY?: string;  
    CreateDate?: Date;  
    UpdateDate?: Date;  
    CreateUser?: string;  
    UpdateUser?: string;  
    CHITIEUD?: string;  
    TUONGQUAN?: number;  
    TRONGSO?: number;  
    User?: User[] = [];
    idUser?: string;
    version?: Int16Array;
}