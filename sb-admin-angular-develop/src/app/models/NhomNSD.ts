import { NhomNSD_ChucNang } from "./NhomNSD_ChucNang"
import { User_NhomNSD } from "./User_NhomNSD"

export class NhomNSD {
    id?: number
    ten?: string
    trangthai?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    User_NhomNSDs?: User_NhomNSD[]
    NhomNSD_ChucNang?: NhomNSD_ChucNang[]
}