import { ChucNang } from "./ChucNang"
import { NhomNSD } from "./NhomNSD"

export class NhomNSD_ChucNang {
    constructor(chucnang_id: number) {
        this.chucnang_id = chucnang_id;
    }

    int?: number
    nhomnsd_id?: number
    chucnang_id?: number
    NhomNSDs?: NhomNSD
    ChucNangs?: ChucNang
}