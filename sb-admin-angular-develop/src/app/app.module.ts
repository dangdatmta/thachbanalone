// import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth.guard';

@NgModule({

    // imports: [BrowserModule, AppRoutingModule, HttpClientModule, Ng2OrderModule],
    imports: [ AppRoutingModule,BrowserModule,BrowserAnimationsModule,HttpClientModule,CommonModule],
    declarations:[AppComponent],
    providers: [AuthGuard],
    bootstrap: [AppComponent],
})
export class AppModule {}
