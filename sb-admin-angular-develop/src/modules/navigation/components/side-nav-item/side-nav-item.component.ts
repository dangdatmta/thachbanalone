import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BoPhan } from '@app/models/BoPhan';
import { ConfigReport } from '@app/models/ConfigReport';
import { AuthService } from '@modules/auth/services';
import { SBRouteData, SideNavItem } from '@modules/navigation/models';
import { NavigationService } from '@modules/navigation/services';
// import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'sb-side-nav-item',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './side-nav-item.component.html',
    styleUrls: ['side-nav-item.component.scss'],
})
export class SideNavItemComponent implements OnInit {
    @Input() bophanid!: string;
    @Input() sideNavItem!: SideNavItem;
    @Input() isActive!: boolean;

    expanded = false;
    routeData!: SBRouteData;
    bophan: BoPhan[] = []
    bophanMenu: BoPhan[] = []
    currentUrl?: string
    id?: number
    Tests?: ConfigReport[]
    constructor(private authService: AuthService, private serverHttp: NavigationService, private router: Router) { }
    roleDetail: string[] = []
    roleName: string[] = []
    bpid!: number;
    idUser!: number;
    ngOnInit() {
        this.idUser = Number.parseInt(localStorage.getItem('UserId') as string);
        this.roleDetail = this.authService.getRoleDetail()
        this.roleName = this.authService.getRoleName()
        this.getALLBoPhan()
        this.getALLBoPhanForSubMenu()
        this.checkChucNang();
        this.checkChucNangImport();
        
    }
    Logout(){
        localStorage.removeItem('userToken')
        this.router.navigate(['/auth/login'])
    }
    checkchucnang!: boolean;
    checkchucnangimport!: boolean;
    checkChucNang() {
      if (this.authService.getTenChucNang().includes("Xem báo cáo")) {
        this.checkchucnang = true
      }
      else {
        this.checkchucnang = false
      }
    }
    checkChucNangImport() {
        if (this.authService.getTenChucNang().includes("Upload dữ liệu")) {
          this.checkchucnangimport = true
        }
        else {
          this.checkchucnangimport = false
        }
      }
    bophanmenunhapdulieu!: BoPhan[];
    getALLBoPhan() {
        // this.serverHttp.getBoPhans().subscribe((data) => {
        //     this.bophan = data
        //     console.log('bophan',data)
        // });
        this.authService.checkShowHideBoPhan(this.idUser).subscribe((data) => {
            data.forEach(element => {
                this.bophan.push(element)
            });
        });
    }
    getALLBoPhanForSubMenu() {
        this.currentUrl = this.router.url
        this.serverHttp.getBoPhansForSubmenu(this.idUser).subscribe((data) => {
            data.forEach(element => {
                element[0].link = this.currentUrl;
                this.bophanMenu.push(element);
            });
        });
    }

    getCauHinhByBoPhanID(bophanID: number){
        this.serverHttp.getCauHinhByBoPhanID(bophanID).subscribe((data) => {
            this.Tests = data
        });
    }
    idBoPhan?:number
    getIdMenu(submenuItem: any){
        this.idBoPhan = submenuItem.id
        this.bophanid = submenuItem.id
        this.authService.setIdBoPhan(submenuItem.id)
    }

    idMenulv2?:number
    getIdMenuLv2(submenuItemLv2: ConfigReport){
        this.idMenulv2 = submenuItemLv2.id
    }

    onSubMenuClick(value: Partial<SideNavItem>) {
        this.authService.setIdEmbed(value.id)
        // console.log("==== ", value.id);
    }
}
