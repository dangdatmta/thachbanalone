import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarSubnavComponent } from './sidebar-subnav.component';

describe('SidebarSubnavComponent', () => {
  let component: SidebarSubnavComponent;
  let fixture: ComponentFixture<SidebarSubnavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarSubnavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarSubnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
