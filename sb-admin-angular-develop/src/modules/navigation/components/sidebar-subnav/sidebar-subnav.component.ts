import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { AuthService } from '@modules/auth/services';
import { SideNavItem } from '@modules/navigation/models';

@Component({
  selector: 'sb-sidebar-subnav',
  templateUrl: './sidebar-subnav.component.html',
  styleUrls: ['./sidebar-subnav.component.scss']
})
export class SidebarSubnavComponent implements OnInit {
  id!: number
  @Input() menuItems: SideNavItem[] = [];
  @Input() isActive!: boolean;
  @Output() onMenuClick = new EventEmitter<{ id: number }>();
  expanded = false;

  constructor(private authservice: AuthService) { }

  // ngOnChanges(changes: SimpleChanges): void {
  //   console.log(changes);
  // }

  ngOnInit(): void {
      this.id = this.authservice.getIdEmbed();
  }

}
