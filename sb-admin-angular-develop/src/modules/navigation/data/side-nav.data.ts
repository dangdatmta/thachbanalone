import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSections: SideNavSection[] = [
    {
        text: 'CHỨC NĂNG',
        items: ['reports', 'layouts', 'pages'],
    },
    // {
    //     text: 'NGƯỜI DÙNG',
    //     items: ['user'],
    // },
];

export const sideNavItems: SideNavItems = {
    // dashboard: {
    //     icon: 'tachometer-alt',
    //     text: 'Dashboard',
    //     link: '/dashboard',
    // },
    reports: {
        icon: '',
        text: 'BÁO CÁO',
        // link: '/auth/Embed',
        ConfigReports: [
        ],
    },
    layouts: {
        icon: '',
        text: 'NHẬP DỮ LIỆU',
        ConfigReports: [

        ],
    },
    pages: {
        icon: '',
        text: 'QUẢN TRỊ',
        ConfigReports: [
            {
                text: 'Nhóm nghiệp vụ',
                link: '/auth/bophan',
            },
            {
                text: 'Master Data',
                ConfigReports: [
                    {
                        text: 'Khu vực',
                        link: '/auth/khuvuc',
                    },
                    {
                        text: 'Tỉnh thành',
                        link: '/auth/tinhthanh',
                    },
                    {
                        text: 'Đại lý',
                        link: '/auth/daily',
                    },
                    {
                        text: 'Sản phẩm',
                        link: '/auth/sanpham',
                    },
                    {
                        text: 'Màu sắc',
                        link: '/auth/mausac',
                    },
                ],
            },
            {
                icon: '',
                text: 'Quản trị người dùng',
                ConfigReports: [
                    {
                        text: 'Thêm và phân quyền người dùng',
                        link: '/auth/quan-tri-nguoi-dung',
                    },
                    {
                        text: 'Quản trị nhóm người sử dụng',
                        link: '/auth/nhomnguoisudung',
                    },
                ],
            },
            {
                text: 'Quản trị view báo cáo',
                ConfigReports: [
                    {
                        text: 'Cấu hình báo cáo',
                        link: '/auth/configreport',
                    },
                    // {
                    //     text: 'Nhóm view báo cáo',
                    //     link: '/auth/nhomviewbaocao',
                    // },
                ],
            },
            {
                text: 'Quản trị nhập dữ liệu',
                ConfigReports: [
                    {
                        text: 'Quản trị kỳ báo cáo',
                        link: '/auth/kybaocao',
                    },
                    {
                        text: 'Đóng mở kỳ báo cáo',
                        link: '/auth/dongmoky',
                    },
                    {
                        text: 'Nhóm loại dữ liệu',
                        link: '/auth/nhomloaidulieu',
                    },

                ],
            },
            // {
            //     text: 'Quản trị nhóm quyền',
            //     link: '/auth/quan-tri-nhom-quyen',
            // },
            // {
            //     text: 'Error',
            //     submenu: [
            //         {
            //             text: '401 Page',
            //             link: '/error/401',
            //         },
            //         {
            //             text: '404 Page',
            //             link: '/error/404',
            //         },
            //         {
            //             text: '500 Page',
            //             link: '/error/500',
            //         },
            //     ],
            // },
        ],
    },
    // user: {
    //     icon: '',
    //     text: 'NGƯỜI DÙNG',
    //     ConfigReports: [
           
    //     ],
    // },
};
