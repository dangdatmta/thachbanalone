import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { Color } from '@app/models/Color';
import { ConfigReport } from '@app/models/ConfigReport';
import { DaiLy } from '@app/models/DaiLy';
import { KyBaoCao } from '@app/models/KyBaoCao';
import { LoginForm } from '@app/models/LoginForm';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { NhomNSD } from '@app/models/NhomNSD';
import { NLDL_KyBC } from '@app/models/NLDL_KyBC';
import { Role } from '@app/models/Role';
import { SanPham } from '@app/models/SanPham';
import { User } from '@app/models/User';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class AuthService {
    idEmbed?: number;
    userName?: string
    roleDetail: string[] = []
    role: string[] = []
    boPhanDetail: string[] = []
    boPhan: string[] = []
    nhomNSD: string[] = []
    bophan_id?: number
    user_id?: number
    tenchucnangs: string[] = []
    tenchucnangtam!: string
    NLDL = new BehaviorSubject<any[]>([]);
    setIdEmbed(data) {
        this.idEmbed = data;
    }
    getIdEmbed() {
        return this.idEmbed as number;
    }
    setTenChucNang(data) {
        this.tenchucnangs = data;
        // data.forEach(element => {
        //     this.tenchucnangtam = element.ten
        //     this.tenchucnangs.push(this.tenchucnangtam)
        // });
    }
    getTenChucNang() {
        // [JSON.stringify(localStorage.getItem('tenchucnang') as string)].forEach(([key, value]) => {
        //     if(key=="ten")
        //     {
        //         this.tenchucnangtam = value
        //         this.tenchucnangs.push(this.tenchucnangtam)
        //     }
        // });
        return localStorage.getItem('tenchucnang') as string;
    }
    setIdUser(data) {
        this.user_id = data
    }
    getIdUser() {
        return Number.parseInt(localStorage.getItem('UserId') as string)
    }
    setIdBoPhan(data) {
        this.bophan_id = data
        this.fetchNLDLByBoPhanID().subscribe((data) => this.NLDL.next(data));
    }
    getIdBoPhan() {
        return this.bophan_id
    }
    setNhomNSDName(data) {
        this.nhomNSD = data
    }
    getNhomNSDName() {
        return this.nhomNSD
    }
    setBoPhanName(data) {
        this.boPhan = data
    }
    getBoPhanName() {
        return this.boPhan
    }
    setBoPhanDetail(data) {
        this.boPhanDetail = data
    }
    getNhomNSDDetail() {
        return this.boPhanDetail
    }
    setNhomNSDDetail(data) {
        this.boPhanDetail = data
    }
    getBoPhanDetail() {
        return this.boPhanDetail
    }
    setRoleName(data) {
        this.role = data
    }
    getRoleName() {
        return this.role
    }
    setRoleDetail(data) {
        this.roleDetail = data
    }
    getRoleDetail() {
        return this.roleDetail
    }
    setUserName(data) {
        this.userName = data
    }
    getUserName() {
        return localStorage.getItem('userName') as string
    }
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            //Authorization: 'my-auth-token'
        })
    };

    private REST_API_SERVER = "http://192.168.62.114:4300/api"
    constructor(private httpClient: HttpClient) { }

    //Users
    public getUsers(): Observable<any> {
        const url = `${this.REST_API_SERVER}/user`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addUser(data: User): Observable<any> {
        const url = `${this.REST_API_SERVER}/user`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateUser(userId?: number, data?: User) {
        const url = `${this.REST_API_SERVER}/user/` + userId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteUser(userId: number) {
        const url = `${this.REST_API_SERVER}/user/` + userId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //Role
    public getRole(): Observable<any> {
        const url = `${this.REST_API_SERVER}/role`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addRole(data: Role): Observable<any> {
        const url = `${this.REST_API_SERVER}/role`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public updateRole(roleId: number, data: Role) {
        const url = `${this.REST_API_SERVER}/role/` + roleId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteRole(roleId: number) {
        const url = `${this.REST_API_SERVER}/role/` + roleId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //khu vực
    public getRegion(): Observable<any> {
        const url = `${this.REST_API_SERVER}/khuvuc`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getRegionById(vungmienID: number) {
        const url = `${this.REST_API_SERVER}/khuvuc/` + vungmienID;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //tỉnh thành
    public getTinhThanhs(): Observable<any> {
        const url = `${this.REST_API_SERVER}/tinhthanh`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //login
    public userAuthentication(loginForm: User) {
        const url = `${this.REST_API_SERVER}/auth/login`;
        const body: User = {
            tendangnhap: loginForm.tendangnhap,
            matkhau: loginForm.matkhau
        }
        return this.httpClient
            .post<any>(url, body, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //bao cao TCKT
    public getListVersion(nldl_id: number, kybc_id: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/excel/` + nldl_id + "," + kybc_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getTCKT01(): Observable<any> {
        const url = `${this.REST_API_SERVER}/excel`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getTCKT01_ByVersion(version?: number, nldl_id?: number, kybc_id?: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/excel/` + version + "," + nldl_id + "," + kybc_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public UploadExcel(formData: FormData, nldl_id: number, kybc_id: number) {
        const url = `${this.REST_API_SERVER}/excel/` + nldl_id + "," + kybc_id;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const httpOptions = { headers: headers };
        return this.httpClient.post(url, formData, httpOptions)
    }
    public DownloadExcel(nldl_id: number) {
        const url = `${this.REST_API_SERVER}/downloadtemplate/` + nldl_id;
        // let headers = new HttpHeaders();
        // headers.append('responseType', 'arraybuffer');
        // const httpOptions = { headers: headers };
        return this.httpClient
            .get(url, { responseType: 'arraybuffer' })
            .pipe(catchError(this.handleError));
    }
    public getNameFileToDownload(nldl_id: number) {
        const url = `${this.REST_API_SERVER}/downloadtemplate/getname/` + nldl_id;
        // let headers = new HttpHeaders();
        // headers.append('responseType', 'arraybuffer');
        // const httpOptions = { headers: headers };
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public OverrideVersion(version?: number) {
        const url = `${this.REST_API_SERVER}/excel/` + version;
        return this.httpClient
            .post<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //bao cao KD01CS
    public getListVersionKD01CS(nldl_id: number, kybc_id: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/kd01cs/` + nldl_id + "," + kybc_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getKD01CS(): Observable<any> {
        const url = `${this.REST_API_SERVER}/kd01cs`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getKD01CS_ByVersion(version?: number, nldl_id?: number, kybc_id?: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/kd01cs/` + version + "," + nldl_id + "," + kybc_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public UploadExcelKD01CS(formData: FormData, nldl_id: number, kybc_id: number) {
        const url = `${this.REST_API_SERVER}/kd01cs/` + nldl_id + "," + kybc_id;
        let nguoitao = JSON.stringify(localStorage.getItem('userName'));
        formData.append('nguoitao', nguoitao)
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const httpOptions = { headers: headers };
        return this.httpClient.post(url, formData, httpOptions)
    }
    public DownloadExcelKD01CS(nldl_id: number) {
        const url = `${this.REST_API_SERVER}/downloadtemplate/` + nldl_id;
        // let headers = new HttpHeaders();
        // headers.append('responseType', 'arraybuffer');
        // const httpOptions = { headers: headers };
        return this.httpClient
            .get(url, { responseType: 'arraybuffer' })
            .pipe(catchError(this.handleError));
    }
    
    public OverrideVersionKD01CS(version?: number) {
        const url = `${this.REST_API_SERVER}/kd01cs/` + version;
        return this.httpClient
            .post<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //Nhóm loại dữ liệu
    public getNLDL(): Observable<any> {
        const url = `${this.REST_API_SERVER}/nhomloaidulieu`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public fetchNLDLByBoPhanID(): Observable<any> {
        const url = `${this.REST_API_SERVER}/nhomloaidulieu/` + this.bophan_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getNLDLByBoPhanID(): Observable<any> {
        return this.NLDL;
    }

    public addNLDL(formData: FormData, data: NhomLoaiDuLieu): Observable<any> {
        const url = `${this.REST_API_SERVER}/nhomloaidulieu`;
        let code = JSON.stringify(data.code)
        let name = JSON.stringify(data.name)
        let loaiky = JSON.stringify(data.loaiky)
        let bophan_id = JSON.stringify(data.bophan_id)
        formData.append('code', code)
        formData.append('name', name)
        formData.append('loaiky', loaiky)
        formData.append('bophan_id', bophan_id)
        let headerss = new HttpHeaders();
        headerss.append('Content-Type', 'multipart/form-data');
        headerss.append('Accept', 'application/json');
        const httpOptionss = { headers: headerss };
        return this.httpClient
            .post<any>(url, formData, httpOptionss)
            .pipe(catchError(this.handleError));
    }
    public updateNLDL(nldlId: number,formData: FormData, data: NhomLoaiDuLieu) {
        const url = `${this.REST_API_SERVER}/nhomloaidulieu/` + nldlId;
        let code = JSON.stringify(data.code)
        let name = JSON.stringify(data.name)
        let loaiky = JSON.stringify(data.loaiky)
        let bophan_id = JSON.stringify(data.bophan_id)
        formData.append('code', code)
        formData.append('name', name)
        formData.append('loaiky', loaiky)
        formData.append('bophan_id', bophan_id)
        let headerss = new HttpHeaders();
        headerss.append('Content-Type', 'multipart/form-data');
        headerss.append('Accept', 'application/json');
        const httpOptionss = { headers: headerss };
        return this.httpClient
            .put<any>(url, formData, httpOptionss)
            .pipe(catchError(this.handleError));
    }
    public deleteNLDL(nldlId: number) {
        const url = `${this.REST_API_SERVER}/nhomloaidulieu/` + nldlId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //Bộ phận (Menu1)
    public checkShowHideBoPhan(userid: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/bophan/` + userid;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getBoPhans(): Observable<any> {
        const url = `${this.REST_API_SERVER}/bophan`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addBoPhan(data: BoPhan): Observable<any> {
        const url = `${this.REST_API_SERVER}/bophan`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateBoPhans(bpId?: number, data?: BoPhan) {
        const url = `${this.REST_API_SERVER}/bophan/` + bpId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteBoPhan(bpId: number) {
        const url = `${this.REST_API_SERVER}/bophan/` + bpId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //ConfigReport (Menu2)
    public getConfigReports(): Observable<any> {
        const url = `${this.REST_API_SERVER}/cauhinh`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public addConfigReport(data: ConfigReport): Observable<any> {
        const url = `${this.REST_API_SERVER}/cauhinh`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateConfigReport(cfrpId?: number, data?: ConfigReport) {
        const url = `${this.REST_API_SERVER}/cauhinh/` + cfrpId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteConfigReport(cfrpId: number) {
        const url = `${this.REST_API_SERVER}/cauhinh/` + cfrpId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //quản trị kỳ báo cáo
    public getPeriods(): Observable<any> {
        const url = `${this.REST_API_SERVER}/kybaocao`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addPeriod(year: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/kybaocao/` + year;
        return this.httpClient
            .post<any>(url, year, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updatePeriod(kbcId?: number, data?: KyBaoCao) {
        const url = `${this.REST_API_SERVER}/kybaocao/` + kbcId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deletePeriod(kbcId: number) {
        const url = `${this.REST_API_SERVER}/kybaocao/` + kbcId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getKyFollowNLDL_id(nldl_id: number) {
        const url = `${this.REST_API_SERVER}/kybaocao/` + nldl_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getTrangThai(nldl_id: number, kybc_id: number) {
        const url = `${this.REST_API_SERVER}/kybaocao/getTrangThai/` + nldl_id + "," + kybc_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public DongMoKy(nldl_id: number, kybc_id: number) {
        const url = `${this.REST_API_SERVER}/kybaocao/` + nldl_id + "," + kybc_id;
        return this.httpClient
            .put<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public CheckKy(nldl_id: number, kybc_id: number) {
        const url = `${this.REST_API_SERVER}/kybaocao/checkKy/` + nldl_id + "," + kybc_id;
        return this.httpClient
            .get(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //nhomloaidulieu_kybc(dongmokyver2)
    public getNLDL_KyBC(): Observable<any> {
        const url = `${this.REST_API_SERVER}/kybaocao/nldl_kybc`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public updateNLDL_KyBC(nldlkbcId?: number){
        const url = `${this.REST_API_SERVER}/kybaocao/nldl_kybc/` + nldlkbcId;
        return this.httpClient
            .put<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //FOR FILTER IMPORT DATA
    public getYears(): Observable<any> {
        const url = `${this.REST_API_SERVER}/kybaocao/year`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getLoaiKys(year: number, nldl_id: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/kybaocao/` + year + "," + nldl_id;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //TEST DOWNLOAD FILE MẪU EXCEL
    // public url = '...';
    // download() {
    //     const options = new RequestOptions({
    //         responseType: ResponseContentType.Blob
    //     });

    //     return this.httpClient.get(this.url, options);
    // }

    //nhóm nsd
    // nhóm nsd

    public getNhomNSDID(nhomNSDId: number) {
        const url = `${this.REST_API_SERVER}/nhomNSD/` + nhomNSDId;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getNhomNSD(): Observable<any> {
        const url = `${this.REST_API_SERVER}/nhomNSD`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addNhomNSD(data: NhomNSD): Observable<any> {
        const url = `${this.REST_API_SERVER}/nhomNSD`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateNhomNSD(nhomNSDId: number, data: NhomNSD) {
        const url = `${this.REST_API_SERVER}/nhomNSD/` + nhomNSDId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteNhomNSD(nhomNSDId: number) {
        const url = `${this.REST_API_SERVER}/nhomNSD/` + nhomNSDId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //dai ly
    public getDaiLys(): Observable<any> {
        const url = `${this.REST_API_SERVER}/daily`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //sanpham
    public getSanPhamID(colorID: number) {
        const url = `${this.REST_API_SERVER}/sanpham/` + colorID;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getSanPham(): Observable<any> {
        const url = `${this.REST_API_SERVER}/sanpham`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addSanPham(data: SanPham): Observable<any> {
        const url = `${this.REST_API_SERVER}/sanpham`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateSanPham(colorID: number, data: SanPham) {
        const url = `${this.REST_API_SERVER}/sanpham/` + colorID;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteSanPham(colorID: number) {
        const url = `${this.REST_API_SERVER}/sanpham/` + colorID;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    //dai ly
    public getDaiLyID(dailyID: number) {
        const url = `${this.REST_API_SERVER}/daily/` + dailyID;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getDaiLy(): Observable<any> {
        const url = `${this.REST_API_SERVER}/daily`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addDaiLy(data: DaiLy): Observable<any> {
        const url = `${this.REST_API_SERVER}/daily`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateDaiLy(dailyID: number, data: DaiLy) {
        const url = `${this.REST_API_SERVER}/daily/` + dailyID;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteDaiLy(dailyID: number) {
        const url = `${this.REST_API_SERVER}/daily/` + dailyID;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //mau sac
    public getColorID(colorID: number) {
        const url = `${this.REST_API_SERVER}/mausac/` + colorID;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getColor(): Observable<any> {
        const url = `${this.REST_API_SERVER}/mausac`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addColor(data: Color): Observable<any> {
        const url = `${this.REST_API_SERVER}/mausac`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateColor(colorID: number, data: Color) {
        const url = `${this.REST_API_SERVER}/mausac/` + colorID;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteColor(colorID: number) {
        const url = `${this.REST_API_SERVER}/mausac/` + colorID;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //Validate Excel
    public validate(formData: FormData) {
        const url = `${this.REST_API_SERVER}/excel/validate`;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const httpOptions = { headers: headers };
        return this.httpClient.post(url, formData, httpOptions)
    }
    public getValidateTCKT01() {
        const url = `${this.REST_API_SERVER}/excel/validate`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public validateKD01CS(formData: FormData) {
        const url = `${this.REST_API_SERVER}/kd01cs/validate`;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const httpOptions = { headers: headers };
        return this.httpClient.post(url, formData, httpOptions)
    }
    public getValidateKD01CS() {
        const url = `${this.REST_API_SERVER}/kd01cs/validate`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //Trả về list idBoPhan theo idUser để checked or unchecked để modify người dùng
    public getBoPhanIDFollowUserID(userId: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/user/getbophanid/` + userId;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getNhomNSDIDFollowUserID(userId: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/user/getnnsdid/` + userId;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //chuc nang
    public getChucNangs(): Observable<any> {
        const url = `${this.REST_API_SERVER}/chucnang`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getChucNangIDFollowNhomNSDID(nnsdId: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/chucnang/getchucnangid/` + nnsdId;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //get key for trust tableau
    public getKeyTrust(){
        const url = `${this.REST_API_SERVER}/embed`;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const httpOptions = { headers: headers };
        return this.httpClient
            .post(url,httpOptions)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // Return an observable with a user-facing error message.
        return throwError(
            'Something bad happened; please try again later.');
    }
    getAuth$(): Observable<{}> {
        return of({});
    }
}
