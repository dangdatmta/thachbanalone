import { AfterViewInit, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Region } from '@app/models/Region';
import { AuthService } from '@modules/auth/services';
import { Message } from 'primeng/api';

@Component({
  selector: 'sb-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
})
export class RegionComponent implements OnInit {
  searchValue?: string
  EditDialog?: boolean
  submitted?: boolean
  id?: string
  public Region: Region[] = []
  region?: Region
  statuses?: any[];
  constructor(
    public sanitizer: DomSanitizer,
    private serverHttp: AuthService
  ) { this.sanitizer = sanitizer; }

  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào khu vực' }];
    this.loadAllRegion();
    this.checkChucNang()
  }
  checkchucnang!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Xem | KV")) {
      this.checkchucnang = true
    }
    else {
      this.checkchucnang = false
    }
  }
  loadAllRegion() {
    this.serverHttp.getRegion().subscribe((data) => {
      this.Region = data;
    });
  }
  getRegionById(id) {
    this.EditDialog = true;
    this.serverHttp.getRegionById(id).subscribe((data) => {
    });
  }
  edit(region: Region) {
    this.region = { ...region };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
}
