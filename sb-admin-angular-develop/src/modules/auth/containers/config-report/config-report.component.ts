import { Component, Inject, NgModule, OnInit } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { ConfigReport } from '@app/models/ConfigReport';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { UserRole } from '@app/models/UserRole';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'sb-config-report',
  templateUrl: './config-report.component.html',
  styleUrls: ['./config-report.component.scss']
})
export class ConfigReportComponent implements OnInit {
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean

  ConfigReports: ConfigReport[] = []
  configreport?: ConfigReport
  BoPhans: BoPhan[] = []

  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    @Inject(DOCUMENT) private _document: Document) { }
  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào cấu hình báo cáo' }];
    this.getAll()
    this.getBaoCaos()
    this.checkChucNang()
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | CHBC")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | CHBC")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | CHBC")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if(this.serverHttp.getTenChucNang().includes("Thêm | CHBC") || this.serverHttp.getTenChucNang().includes("Sửa | CHBC") || this.serverHttp.getTenChucNang().includes("Xóa | CHBC")){
      this.checkXem = true
    }else{
      this.checkXem = false
    }
  }
  getAll() {
    this.serverHttp.getConfigReports().subscribe((data) => {
      this.ConfigReports = data;
    });
  }
  // LoadMenu(){
  //   this.serverHttp.getConfigReports().subscribe((data) => {
  //   });
  // }
  getBaoCaos() {
    this.serverHttp.getBoPhans().subscribe((data) => {
      this.BoPhans = data;
      this.serverHttp.getConfigReports().subscribe((data) => {
      });
    });
  }
  openNew() {
    this.configreport = {};
    this.configreport.trangthai = 1;
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(configreport: ConfigReport) {
    this.configreport = { ...configreport };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    if(this.configreport?.tableurl_id?.trim() && this.configreport.text?.trim() && this.configreport.bophan_id){
      if (this.configreport?.id) {
        this.ConfigReports[this.findIndexById(this.configreport.id)] = this.configreport;
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
        this.serverHttp.updateConfigReport(this.configreport.id, this.configreport).subscribe((data) => { }
        );
        this.configreport = {};
      }
      else {
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tạo thành công', life: 3000 });
        this.serverHttp.addConfigReport(this.configreport as ConfigReport).subscribe((data) => {
          // this.getBaoCaos()
          // this.getAll()
          this.serverHttp.getBoPhans().subscribe((data) => {
            this.BoPhans = data;
            this.serverHttp.getConfigReports().subscribe((data) => {
              this.ConfigReports = data;
            });
          });
  
        });
  
        this.configreport = {};
      }
  
      this.ConfigReports = [...this.ConfigReports];
      this.EditDialog = false;
      this.refreshPage()
    }
  }

  refreshPage() {
    this._document.defaultView?.location.reload()
  }

  findIndexById(id?: number): number {
    let index = -1;
    for (let i = 0; i < this.ConfigReports.length; i++) {
      if (this.ConfigReports[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(cfrp: ConfigReport) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + cfrp.text + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteConfigReport(cfrp.id as number).subscribe((data) => { }
        );
        this.ConfigReports = this.ConfigReports.filter(val => val.id !== cfrp.id);
        this.configreport = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
        this.refreshPage()
      }
    });

  }
}
