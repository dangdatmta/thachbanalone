import { Component, NgModule, OnInit } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { User } from '@app/models/User';
import { UserRole } from '@app/models/UserRole';
import { User_NhomNSD } from '@app/models/User_NhomNSD';
import { NhomNSD } from '@app/models/NhomNSD';
import { User_BoPhan } from '@app/models/User_BoPhan';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { Region } from '@app/models/Region';
import { TinhThanh } from '@app/models/TinhThanh';
import { SanPham } from '@app/models/SanPham';
import { DaiLy } from '@app/models/DaiLy';
import { Color } from '@app/models/Color';

@Component({
  selector: 'sb-quan-tri-nguoi-dung',
  templateUrl: './quan-tri-nguoi-dung.component.html',
  styleUrls: ['./quan-tri-nguoi-dung.component.scss']
})
export class QuanTriNguoiDungComponent implements OnInit {
  isCheck?: boolean
  boPhanDetail: string[] = []
  nhomNSDDetail: string[] = []
  boPhanName: string[] = []
  nhomNSDName: string[] = []
  FullName?: string
  roles: UserRole[] = []
  userBoPhan: User_BoPhan[] = []
  userNhomNSD: User_NhomNSD[] = []
  titleDialog?: boolean
  boPhanSelected?: number
  nhomNSDSelected?: number
  User: User[] = []
  BoPhan: BoPhan[] = []
  NhomNSD: NhomNSD[] = []
  nhomNSD?: NhomNSD
  boPhan?: BoPhan
  userRoles: UserRole[] = []
  userBoPhans: User_BoPhan[] = []
  userNhomNSDs: User_NhomNSD[] = []
  boPhans: BoPhan[] = []

  user?: User
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean
  EditDialog2?: boolean

  KhuVucs?: Region[]
  TinhThanhs?: TinhThanh[]
  DaiLys?: DaiLy[]
  SanPhams?: SanPham[]
  MauSacs?: Color[]

  //update


  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }
  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào người dùng' }];
    this.FullName = this.serverHttp.getUserName()
    this.boPhanDetail = this.serverHttp.getBoPhanDetail()
    this.nhomNSDDetail = this.serverHttp.getNhomNSDDetail()
    this.boPhanName = this.serverHttp.getBoPhanName()
    this.nhomNSDName = this.serverHttp.getNhomNSDName()
    // console.log('length', this.boPhanName.length)
    this.getAllUser()
    this.getAllBoPhan()
    this.getAllNhomNSD()
    this.boPhanSelected = this.BoPhan.length
    this.nhomNSDSelected = this.NhomNSD.length
    this.roles = new Array<UserRole>();
    this.userNhomNSD = new Array<User_NhomNSD>();
    this.userBoPhan = new Array<User_BoPhan>();
    this.getDaiLys()
    this.getSanPhams()
    this.getMauSacs()
    this.getKhuVucs()
    this.getTinhThanhs()
    this.checkChucNang()

    this.serverHttp.getBoPhans().subscribe((data) => {
      this.boPhans = data;
      for(let i = 1; i < this.boPhans.length+2; i++){
        this.checkbophan.push(this.boPhans.length-i);
      }
      console.log('checkbophan',this.checkbophan)
    });

    this.serverHttp.getNhomNSD().subscribe((data) => {
      this.NhomNSD = data;
      for(let i = 1; i < this.NhomNSD.length+2; i++){
        this.checknnsd.push(this.NhomNSD.length-i);
      }
      console.log('checknnsd',this.checknnsd)
    });
    
  }
  checkchucnangThemvaphanquyen!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkchucnangphanquyentheobc!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm & phân quyền | ND")) {
      this.checkchucnangThemvaphanquyen = true
    }
    else {
      this.checkchucnangThemvaphanquyen = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | ND")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | ND")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if (this.serverHttp.getTenChucNang().includes("Phân theo nhóm Filter Báo cáo BI | ND")) {
      this.checkchucnangphanquyentheobc = true
    }
    else {
      this.checkchucnangphanquyentheobc = false
    }
    if (this.serverHttp.getTenChucNang().includes("Thêm & phân quyền | ND")
      || this.serverHttp.getTenChucNang().includes("Sửa | ND")
      || this.serverHttp.getTenChucNang().includes("Xóa | ND")
      || this.serverHttp.getTenChucNang().includes("Phân theo nhóm Filter Báo cáo BI | ND")) {
      this.checkXem = true
    } else {
      this.checkXem = false
    }
  }
  selectedBoPhans: any = [];
  selectedNNSDs: any = [];
  idbophans: number[] = [];
  idnnsds: number[] = [];
  getBoPhanIDFollowUserID(userId) {
    this.serverHttp.getBoPhanIDFollowUserID(userId).subscribe((data) => {
      console.log('data',data)
      this.idbophans = data;
      this.selectedBoPhans = data;
      data.forEach(element => {
        this.userBoPhans.push(new User_BoPhan(element));
      });
    });
  }

  getNhomNSDIDFollowUserID(userId){
    this.serverHttp.getNhomNSDIDFollowUserID(userId).subscribe((data) => {
      console.log('data',data)
      this.idnnsds = data;
      this.selectedNNSDs = data;
      data.forEach(element => {
        this.userNhomNSDs.push(new User_NhomNSD(element));
      });
    });
  }

  getAllUser() {
    this.serverHttp.getUsers().subscribe((data) => {
      // console.log('data', data);
      this.User = data;
    });
  }
  getAllBoPhan() {
    this.serverHttp.getBoPhans().subscribe((data) => {
      // console.log('Bộ phận', data);
      this.BoPhan = data;
    });
  }
  getAllNhomNSD() {
    this.serverHttp.getNhomNSD().subscribe((data) => {
      // console.log('nhóm người sử dụng', data);
      this.NhomNSD = data;
    });
  }

  openNew() {
    this.user = {};
    this.user.trangthai = 1;
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(user: User) {
    this.getBoPhanIDFollowUserID(user.id);
    this.getNhomNSDIDFollowUserID(user.id);
    this.user = { ...user };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
    this.selectedNNSDs = []
    this.userNhomNSDs = []
    this.selectedBoPhans = []
    this.userBoPhans = []
  }
  edit2(user: User) {
    this.user = { ...user };
    this.EditDialog2 = true;
  }
  hideDialog2() {
    this.EditDialog2 = false;
    this.submitted = false;
  }
  save() {
    this.submitted = true;
    if (this.user?.ten?.trim() && this.user?.tendangnhap?.trim() && this.user?.matkhau?.trim()) {
      if (this.user?.id) {
        this.user = { ...this.user, User_BoPhans: this.userBoPhans, User_NhomNSDs: this.userNhomNSDs };
        this.User[this.findIndexById(this.user.id)] = this.user;
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
        this.serverHttp.updateUser(this.user.id, this.user).subscribe((data) => { }
        );
        this.User = [...this.User];
        this.user = {}
        this.userRoles = []
        this.userBoPhans = []
        this.userNhomNSDs = []
        this.EditDialog = false;
      }
      else {
        if (this.user.trangthai == null) {
          this.user.trangthai = 1;
        }
        this.user = { ...this.user, User_BoPhans: this.userBoPhans, User_NhomNSDs: this.userNhomNSDs };
        this.serverHttp.addUser(this.user as User).subscribe((data) => {
          if (data === 'UserName này đã tồn tại!!!') {
            this.messageService.add({ key: 'tr', severity: 'error', summary: 'Thất bại', detail: 'UserName này đã tồn tại', life: 3000 });
          }
          else {
            this.User.push(this.user!)
            this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tạo nhân viên thành công', life: 3000 });
            this.User = [...this.User];
            this.user = {}
            this.userRoles = []
            this.userBoPhans = []
            this.userNhomNSDs = []
            this.EditDialog = false;
          }
        });
      }
    }
  }

  findIndexById(id?: number): number {
    let index = -1;
    for (let i = 0; i < this.User.length; i++) {
      if (this.User[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }
  // getBoPhanId(e: any, id: number) {
  //   if (e.target.checked) {
  //     this.userBoPhans.push(new User_BoPhan(id));
  //   } else {
  //     console.log(id + 'uncheckedBoPhan')
  //     this.userBoPhans = this.userBoPhans.filter(m => m.BoPhan_id != id)
  //   }
  //   console.log(this.userBoPhans)
  // }
  checkbophan: number[] = [];
  getBoPhanId(event) {
    let index = this.selectedBoPhans.indexOf(event.target.value);
   
    console.log('index', index)
    if(event.target.checked){
      if (this.checkbophan.includes(index)) {
        this.selectedBoPhans.push(event.target.value);
        this.userBoPhans.push(new User_BoPhan(event.target.value));
      } else {
        console.log(event.target.value + 'uncheckedBoPhan')
        this.selectedBoPhans.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.userBoPhans = this.userBoPhans.filter(m => m.BoPhan_id != event.target.value)
      }
    }else{
      if (!this.checkbophan.includes(index)) {
        this.selectedBoPhans.push(event.target.value);
        this.userBoPhans.push(new User_BoPhan(event.target.value));
      } else {
        console.log(event.target.value + 'uncheckedBoPhan')
        this.selectedBoPhans.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.userBoPhans = this.userBoPhans.filter(m => m.BoPhan_id != event.target.value)
      }
    }
    console.log(this.userBoPhans)
  }
  checknnsd: number[] = [];
  getNhomNSDId(event) {
    let index = this.selectedNNSDs.indexOf(event.target.value);
   
    console.log('indexnssd', index)
    if(event.target.checked){
      if (this.checknnsd.includes(index)) {
        this.selectedNNSDs.push(event.target.value);
        this.userNhomNSDs.push(new User_NhomNSD(event.target.value));
      } else {
        console.log(event.target.value + 'uncheckednnsd')
        this.selectedNNSDs.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.userNhomNSDs = this.userNhomNSDs.filter(m => m.NhomNSD_id != event.target.value)
      }
    }else{
      if (!this.checknnsd.includes(index)) {
        this.selectedNNSDs.push(event.target.value);
        this.userNhomNSDs.push(new User_NhomNSD(event.target.value));
      } else {
        console.log(event.target.value + 'uncheckednnsd')
        this.selectedNNSDs.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.userNhomNSDs = this.userNhomNSDs.filter(m => m.NhomNSD_id != event.target.value)
      }
    }
    console.log(this.userNhomNSDs)
  }
  // getNhomNSDId(e: any, id: number) {
  //   if (e.target.checked) {
  //     this.userNhomNSDs.push(new User_NhomNSD(id));
  //   } else {
  //     this.userNhomNSDs = this.userNhomNSDs.filter(m => m.NhomNSD_id != id)
  //   }
  // }

  delete(user: User) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + user.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteUser(user.id as number).subscribe((data) => { }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.User = this.User.filter(val => val.id !== user.id);
        this.user = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }

  //
  getKhuVucs() {
    this.serverHttp.getRegion().subscribe((data) => { this.KhuVucs = data });
  }
  getTinhThanhs() {
    this.serverHttp.getTinhThanhs().subscribe((data) => { this.TinhThanhs = data });
  }
  getDaiLys() {
    this.serverHttp.getDaiLys().subscribe((data) => { this.DaiLys = data });
  }
  getSanPhams() {
    this.serverHttp.getSanPham().subscribe((data) => { this.SanPhams = data });
  }
  getMauSacs() {
    this.serverHttp.getColor().subscribe((data) => { this.MauSacs = data });
  }
}
