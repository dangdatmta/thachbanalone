import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseUploadDuLieuComponent } from './base-upload-du-lieu.component';

describe('BaseUploadDuLieuComponent', () => {
  let component: BaseUploadDuLieuComponent;
  let fixture: ComponentFixture<BaseUploadDuLieuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseUploadDuLieuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseUploadDuLieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
