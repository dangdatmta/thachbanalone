import { Component, OnInit } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { Role } from '@app/models/Role';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-bophan',
  templateUrl: './quan-tri-bophan.component.html',
  styleUrls: ['./quan-tri-bophan.component.scss'],
  providers: [MessageService, ConfirmationService]
})

export class QuanTriBophanComponent implements OnInit {
  editDialog?: boolean;
  submitted?: boolean;
  BoPhans: BoPhan[] = []
  bophan?: BoPhan
  constructor(
    private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService
  ) { }
  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào bộ phận' }];
    this.getBoPhans()
    this.checkChucNang()
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | NNV")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | NNV")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | NNV")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if (this.serverHttp.getTenChucNang().includes("Thêm | NNV") || this.serverHttp.getTenChucNang().includes("Sửa | NNV") || this.serverHttp.getTenChucNang().includes("Xóa | NNV")) {
      this.checkXem = true
    } else {
      this.checkXem = false
    }
  }
  getBoPhans() {
    this.serverHttp.getBoPhans().subscribe((data) => {
      this.BoPhans = data;
    });
  }
  edit(bophan: BoPhan) {
    this.bophan = { ...bophan };
    this.editDialog = true;
  }
  openNew() {
    this.bophan = {};
    this.bophan.trangthai = 1;
    this.submitted = false;
    this.editDialog = true;
  }
  hideDialog() {
    this.editDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    if (this.bophan?.code?.trim() && this.bophan.text?.trim()) {
      if (this.bophan?.id) {
        this.BoPhans[this.findIndexById(this.bophan.id)] = this.bophan;
        this.serverHttp.updateBoPhans(this.bophan.id, this.bophan).subscribe((data) => { }
        );
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
        this.bophan = {};
      }
      else {
        if (this.bophan.trangthai == null) {
          this.bophan.trangthai = 1;
        }
        this.serverHttp.addBoPhan(this.bophan as BoPhan).subscribe((data) => {
          // console.log('data', data)
          this.getBoPhans()
        });
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tạo bộ phận thành công', life: 3000 });
        this.bophan = {};
        this.getBoPhans()
      }
      this.BoPhans = [...this.BoPhans];
      this.editDialog = false;
    }


  }

  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.BoPhans.length; i++) {
      if (this.BoPhans[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(bophan: BoPhan) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa bộ phận ' + bophan.text + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {

        this.serverHttp.deleteBoPhan(bophan.id as number).subscribe((data) => {
          if (data == 'Có lỗi xảy ra!!!') {
            this.messageService.add({ key: "tr", severity: 'error', summary: 'Thất bại', detail: 'có lỗi xảy ra ở server, vui lòng liên hệ QTV!', life: 3000 });
          }
          else {
            this.BoPhans = this.BoPhans.filter(val => val.id !== bophan.id);
            this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
            this.bophan = {};
          }
        }
        );
      }
    });
  }
}

