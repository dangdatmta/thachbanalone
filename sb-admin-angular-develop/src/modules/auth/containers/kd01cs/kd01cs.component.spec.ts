import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Kd01csComponent } from './kd01cs.component';

describe('Kd01csComponent', () => {
  let component: Kd01csComponent;
  let fixture: ComponentFixture<Kd01csComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Kd01csComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Kd01csComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
