import { AfterViewInit, ChangeDetectionStrategy, Component, Inject, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { F_CSKH05 } from '@app/models/F_CSKH05';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { TCKT01 } from '@app/models/TCKT.01';
import { AuthService } from '@modules/auth/services';
import { ConfirmationService, Message } from 'primeng/api';
import { MessageService } from 'primeng/api';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver/dist/FileSaver';
import { ValidateTCKT01 } from '@app/models/ValidateTCKT01';
import { KD01CS } from '@app/models/KD01CS';
import { ValidateKD01CS } from '@app/models/ValidateKD01CS';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'sb-kd01cs',
  templateUrl: './kd01cs.component.html',
  styleUrls: ['./kd01cs.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class Kd01csComponent implements OnInit, OnDestroy {
  @ViewChild('fileInput') fileInput;
  destroy$ = new Subject();
  validateDialog?: boolean;
  submitted?: boolean;
  versions: number[] = []
  public isCollapsed = false
  message?: string;
  timeUpdate?: Date;
  userCreate?: string;
  TCKT01: KD01CS[] = []
  countRecords?: number
  version?: number
  maxSTT?: number

  NLDL?: NhomLoaiDuLieu[]
  Year?: number[]
  year?: number
  KyBC?: string[]
  alertNoData?: string
  validateTCKT01s?: ValidateKD01CS[]

  constructor(
    private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private activatedRoute: ActivatedRoute,
    @Inject(DOCUMENT) private _document: Document
  ) { }
  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
  bophan_id?: number
  msg: Message[] = [];

  ngOnInit(): void {
    //this.getAllVersion()
    this.getYears()
    this.getNLDLByBoPhanID()
  }
  checkky!: boolean
  nldl_id?: number;
  kybc_id?: number;
  checkKy(nldl_id, kybc_id) {
    this.serverHttp.CheckKy(nldl_id, kybc_id).subscribe((data) => {
      // console.log('checkky', data)
      this.checkky = data as boolean
      if(!this.checkky){
        this.messageService.add({ key: 'tr', severity: 'info', summary: 'Thông báo', detail: 'Kỳ đang bị khóa, không thể update dữ liệu!', life: 3000 });
      }
      return data;
    });
  }
  refreshPage() {
    this._document.defaultView?.location.reload();
    this.year = this.getYEAR();
  }
  getLoaiKys(year, nldl_id) {
    this.errorMessValidate = []
    this.msg = []
    this.validateTCKT01s = []
    this.serverHttp.getLoaiKys(year, nldl_id).subscribe((data) => {
      this.KyBC = data
      if (data == '') {
        this.msg = [{ severity: 'info', summary: 'Thông báo', detail: 'Không tìm thấy dữ liệu phù hợp.' }];
      }
      // console.log('year', year)
      // console.log('nldl_id', nldl_id)
    });
    this.KyBC = []
  }
  getYEAR(){
    return this.year;
  }
  setYEAR(data){
    this.year = data;
  }
  getYears() {
    this.serverHttp.getYears().subscribe((data) => {
      this.Year = data
      
    });
  }
  getNLDLByBoPhanID() {
    this.serverHttp.getNLDLByBoPhanID().subscribe((data) => {
      this.NLDL = data
    });
  }
  refreshVersion() {
    this.versions.length = 0
  }
  header1?: string;
  header2?: string;
  header3?: string;
  header4?: string;
  header5?: string;
  header6?: string;
  header7?: string;
  header8?: string;
  header9?: string;
  getAllVersion(nldl_id, kybc_id) {
    this.errorMessValidate = []
    this.msg = []
    this.validateTCKT01s = []
    this.serverHttp.getListVersionKD01CS(nldl_id, kybc_id).pipe(takeUntil(this.destroy$)).subscribe((data) => {
      // console.log('version', data);
      this.versions = data;
      if (data != 0) {
        for (var i = this.versions.length - 1; i >= 0; i--) {
          this.serverHttp.getKD01CS_ByVersion(this.versions[i], nldl_id, kybc_id).pipe(takeUntil(this.destroy$)).subscribe((data) => {
            this.TCKT01.push(data)
            this.countRecords = data.length
            this.maxSTT = data[0].stt
            this.header1 = data[0].header1
            this.header2 = data[0].header2
            this.header3 = data[0].header3
            this.header4 = data[0].header4
            this.header5 = data[0].header5
            this.header6 = data[0].header6
            this.header7 = data[0].header7
            this.header8 = data[0].header8
            this.header9 = data[0].header9
          });
        }
      } else {
        this.alertNoData = "Không tìm thấy dữ liệu phù hợp"
        this.msg = [{ severity: 'info', summary: 'Thông báo', detail: 'Không tìm thấy dữ liệu phù hợp.' }];
      }

    });
    // console.log('KYBC', kybc_id)
    // console.log('NDLID', nldl_id)
  }
  overrideVersion(version: number) {
    // console.log('test')
    this.version = version;
    // console.log('ver', this.version)
    this.confirmService.confirm({
      message: 'Dữ liệu sẽ bị thay đổi, bạn có chắc chắn muốn ghi đè?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.OverrideVersionKD01CS(this.version).subscribe(data => {
          
          this.getAllVersion(this.nldl_id, this.kybc_id);
          this.TCKT01 = {...this.TCKT01};
        });

        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Ghi đè dữ liệu thành công', life: 3000 });
        this.refreshPage();
      }
      
    });
  }

  exportexcel(version: number) {
    this.version = version;
    /* pass here the table id */
    let element = document.getElementById(this.version.toString());
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    /* save to file */
    XLSX.writeFile(wb, 'KD01CS.xlsx');
  }

  uploadFile(nldl_id, kybc_id) {
    localStorage.setItem('year',(this.year as number).toString());
    let formData = new FormData();
    formData.append('upload', this.fileInput.nativeElement.files[0])
    this.confirmService.confirm({
      message: 'Bạn có thật sự muốn thêm dữ liệu không?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        if (this.errorMessValidate == "") {
          this.serverHttp.UploadExcelKD01CS(formData, nldl_id, kybc_id).subscribe(result => {
            if (result == "Excel file has been successfully uploaded") {
              this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tải file thành công', life: 3000 });
            }
            else {
              this.messageService.add({ key: 'tr', severity: 'error', summary: 'Thất bại', detail: 'Dữ liệu bạn chọn còn lỗi hoặc định dạng file chưa đúng!!!', life: 3000 });
            }
          });
          this.refreshPage();
          
        }
        else {
          this.messageService.add({ key: 'tr', severity: 'error', summary: 'Thất bại', detail: 'Dữ liệu bạn chọn còn lỗi hoặc định dạng file chưa đúng!!!', life: 3000 });
        }
      }
    });
  }
  errorMessValidate?: any
  validateExcel() {
    this.errorMessValidate = []
    this.msg = []
    this.validateTCKT01s = []
    let formData = new FormData();
    formData.append('upload', this.fileInput.nativeElement.files[0])
    this.serverHttp.validateKD01CS(formData).subscribe(result => {
      if (result == "") {
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Dữ liệu của file không có lỗi', life: 3000 });
        this.serverHttp.getValidateKD01CS().subscribe(data => {
          // console.log('vali', data)
          this.validateTCKT01s = data
        });
      }
      else if (result == "Định dạng file không được hỗ trợ!!!") {
        this.messageService.add({ key: 'tr', severity: 'error', summary: 'Thất bại', detail: 'Định dạng này không được hỗ trợ!!!', life: 3000 });
      }
      else {

        this.errorMessValidate = result;
        this.errorMessValidate.forEach(element => {
          this.msg.push({ severity: 'error', summary: 'Lỗi', detail: element });
        });

        this.messageService.add({ key: 'tr', severity: 'error', summary: 'Thất bại', detail: 'Dữ liệu của file có lỗi!!!', life: 3000 });
        this.serverHttp.getValidateKD01CS().subscribe(data => {
          // console.log('vali', data)
          this.validateTCKT01s = data
        });
      }

    });
  }
  downloadExcel(nldl_id) {
    this.serverHttp.DownloadExcelKD01CS(nldl_id).subscribe((data) => {
      var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
      this.serverHttp.getNameFileToDownload(nldl_id).subscribe((data) => { saveAs(file, data) })
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tải file mẫu thành công', life: 3000 });
    });
  }
  edit() {
    this.validateDialog = true;
  }
  hideDialog() {
    this.validateDialog = false;
    this.submitted = false;
  }
  openNew() {
    this.submitted = false;
    this.validateDialog = true;
  }
  title = 'ThachBanCli';
}
