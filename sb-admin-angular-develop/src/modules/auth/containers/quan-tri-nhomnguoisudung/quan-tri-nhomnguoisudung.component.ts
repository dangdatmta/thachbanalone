import { Component, NgModule, OnInit } from '@angular/core';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { NhomNSD } from '@app/models/NhomNSD';
import { UserRole } from '@app/models/UserRole';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { NhomNSD_ChucNang } from '@app/models/NhomNSD_ChucNang';

@Component({
  selector: 'sb-quan-tri-nhomnguoisudung',
  templateUrl: './quan-tri-nhomnguoisudung.component.html',
  styleUrls: ['./quan-tri-nhomnguoisudung.component.scss']
})
export class QuanTriNhomnguoisudungComponent implements OnInit {

  boPhanDetail: string[] = []
  chucNangDetail: string[] = []
  nhomNSDDetail: string[] = []
  roleName: string[] = []
  FullName?: string
  roles: UserRole[] = []
  titleDialog?: boolean
  roleSelected?: number
  User: User[] = []
  Role: Role[] = []
  NhomNSD: NhomNSD[] = []
  role?: Role
  userRoles: UserRole[] = []
  user?: User
  nhomNSD?: NhomNSD
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean
  file0?: NhomNSD[];
  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
  ) { }
  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào nhóm người sử dụng' }];
    // this.serverHttp.getRoleName().then(role => this.files3 = role);
    this.FullName = this.serverHttp.getUserName()
    this.boPhanDetail = this.serverHttp.getBoPhanDetail()
    // this.groupUserDetail = this.serverHttp.getgroupUserDetail()

    this.roleName = this.serverHttp.getBoPhanName()
    console.log('length', this.roleName.length)
    this.getAllUser()
    // this.getAllRole()
    this.getAllNhomNSD()
    this.roleSelected = this.Role.length
    this.roles = new Array<UserRole>();

    this.checkChucNang()

    this.serverHttp.getChucNangs().subscribe((data) => {
      for (let i = 1; i < data.length + 2; i++) {
        this.checkchucnang.push(data.length - i);
      }
      console.log('checkchucnang', this.checkchucnang)
    });
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | NNSD")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | NNSD")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | NNSD")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if (this.serverHttp.getTenChucNang().includes("Thêm | NNSD") || this.serverHttp.getTenChucNang().includes("Sửa | NNSD") || this.serverHttp.getTenChucNang().includes("Xóa | NNSD")) {
      this.checkXem = true
    } else {
      this.checkXem = false
    }
  }
  getAllUser() {
    this.serverHttp.getUsers().subscribe((data) => {
      console.log('data', data);
      this.User = data;
    });
  }
  // getAllRole() {
  //   this.serverHttp.getRole().subscribe((data) => {
  //     console.log('role', data);
  //     this.Role = data;
  //   });
  // }

  getAllNhomNSD() {
    this.serverHttp.getNhomNSD().subscribe((data) => {
      console.log('data', data);
      this.NhomNSD = data;
    });
  }

  openNew() {
    this.nhomNSD = {};
    this.nhomNSD.trangthai = 1;
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(nhomNSD: NhomNSD) {
    this.getChucNangIDFollowNhomNSDID(nhomNSD.id);
    this.nhomNSD = { ...nhomNSD };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
    this.selectedChucNangs = []
    this.NhomNSD_ChucNang = []
  }

  save() {
    this.submitted = true;
    if (this.nhomNSD?.ten?.trim()) {
      if (this.nhomNSD?.id) {
        this.nhomNSD = { ...this.nhomNSD, NhomNSD_ChucNang: this.NhomNSD_ChucNang };
        this.nhomNSD[this.findIndexById(this.nhomNSD.id)] = this.user;
        this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
        this.serverHttp.updateNhomNSD(this.nhomNSD.id as number, this.nhomNSD).subscribe((data) => { }
        );
        this.NhomNSD = [...this.NhomNSD];
        this.nhomNSD = {}
        this.NhomNSD_ChucNang = []
        this.EditDialog = false;
      }
      else {
        this.nhomNSD = { ...this.nhomNSD, NhomNSD_ChucNang: this.NhomNSD_ChucNang };
        this.serverHttp.addNhomNSD(this.nhomNSD as NhomNSD).subscribe((data) => {
          this.NhomNSD.push(this.nhomNSD!)
          this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tạo thành công', life: 3000 });
          this.NhomNSD = [...this.NhomNSD];
          this.nhomNSD = {}
          this.NhomNSD_ChucNang = []
          this.EditDialog = false;
        });
      }
    }
  }

  findIndexById(id?: number): number {
    let index = -1;
    for (let i = 0; i < this.NhomNSD.length; i++) {
      if (this.NhomNSD[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  getNhomNSDId(id) {
    this.EditDialog = true;
    this.serverHttp.getNhomNSDID(id).subscribe((data) => {
    });
  }

  delete(nhomNSD: NhomNSD) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + nhomNSD.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteNhomNSD(nhomNSD.id as any).subscribe((data) => { }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.NhomNSD = this.NhomNSD.filter(val => val.id !== nhomNSD.id);
        this.nhomNSD = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }

  getChucNangIDFollowNhomNSDID(chucnangID) {
    this.serverHttp.getChucNangIDFollowNhomNSDID(chucnangID).subscribe((data) => {
      console.log('data', data)
      //this.idnnsds = data;
      this.selectedChucNangs = data;
      data.forEach(element => {
        this.NhomNSD_ChucNang.push(new NhomNSD_ChucNang(element));
      });
    });
  }

  NhomNSD_ChucNang: NhomNSD_ChucNang[] = [];
  selectedChucNangs: any = [];
  checkchucnang: number[] = [];
  // getChucNangId(e: any, id: number) {
  //   if (e.target.checked) {
  //     this.NhomNSD_ChucNang.push(new NhomNSD_ChucNang(id));
  //   } else {
  //     console.log(id + 'uncheckedChucNang')
  //     this.NhomNSD_ChucNang = this.NhomNSD_ChucNang.filter(m => m.chucnang_id != id)
  //   }
  //   console.log(this.NhomNSD_ChucNang)
  // }

  getChucNangId(event) {
    let index = this.selectedChucNangs.indexOf(event.target.value);
    if (event.target.checked) {
      if (this.checkchucnang.includes(index)) {
        this.selectedChucNangs.push(event.target.value);
        this.NhomNSD_ChucNang.push(new NhomNSD_ChucNang(event.target.value));
      } else {
        this.selectedChucNangs.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.NhomNSD_ChucNang = this.NhomNSD_ChucNang.filter(m => m.chucnang_id != event.target.value)
      }
    } else {
      if (!this.checkchucnang.includes(index)) {
        this.selectedChucNangs.push(event.target.value);
        this.NhomNSD_ChucNang.push(new NhomNSD_ChucNang(event.target.value));
      } else {
        this.selectedChucNangs.splice(index, 1);
        //this.userBoPhans.splice(index, 1);
        this.NhomNSD_ChucNang = this.NhomNSD_ChucNang.filter(m => m.chucnang_id != event.target.value)
      }
    }
    console.log(this.NhomNSD_ChucNang)
  }
}



