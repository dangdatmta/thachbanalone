import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriNhomnguoisudungComponent } from './quan-tri-nhomnguoisudung.component';

describe('QuanTriNhomnguoisudungComponent', () => {
  let component: QuanTriNhomnguoisudungComponent;
  let fixture: ComponentFixture<QuanTriNhomnguoisudungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriNhomnguoisudungComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriNhomnguoisudungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
