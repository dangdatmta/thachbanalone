import { Component, NgModule, OnInit } from '@angular/core';

import { SanPham } from '@app/models/SanPham';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-sanpham',
  templateUrl: './quan-tri-sanpham.component.html',
  styleUrls: ['./quan-tri-sanpham.component.scss']
})
export class QuanTriSanphamComponent implements OnInit {




  SanPham: SanPham[] = []
  sanPhamDetail: string[] = []
  sanPham?: SanPham
  EditDialog?: boolean
  sanPhamSelected?: boolean
  productDialog?: boolean
  submitted?: boolean
  text?: string

  results?: string[];



  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }

  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào sản phẩm' }];
    this.getAllSanPham()
    this.checkChucNang()
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | SP")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | SP")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | SP")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if(this.serverHttp.getTenChucNang().includes("Thêm | SP")||this.serverHttp.getTenChucNang().includes("Sửa | SP")||this.serverHttp.getTenChucNang().includes("Xóa | SP")){
      this.checkXem = true
    }else{
      this.checkXem = false
    }
  }


  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  getAllSanPham() {
    this.serverHttp.getSanPham().subscribe((data) => {
      this.SanPham = data;
    });
  }
  getSanPhamId(id) {
    this.EditDialog = true;
    this.serverHttp.getSanPhamID(id).subscribe((data) => {
    });
  }
  openNew() {
    this.sanPham = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(sanPham: SanPham) {
    this.sanPham = { ...sanPham };
    this.EditDialog = true;
  }

  save() {
    this.submitted = false;
    if (this.sanPham?.id) {
      this.SanPham[this.findIndexById(this.sanPham.id)] = this.sanPham;
      this.serverHttp.updateSanPham(this.sanPham.id, this.sanPham).subscribe((data) => { }
      );
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.sanPham = {};
      //this.getAllRole()
    }
    else {
      //this.product.id = this.createId();
      this.serverHttp.addSanPham(this.sanPham as SanPham).subscribe((data) => { });
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Thêm mới thành công', life: 3000 });
      this.sanPham = {};
      this.getAllSanPham()
    }
    this.SanPham = [...this.SanPham];
    this.EditDialog = false;

  }
  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.SanPham.length; i++) {
      if (this.SanPham[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(sanPham: SanPham) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + sanPham.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteColor(sanPham.id as any).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.SanPham = this.SanPham.filter(val => val.id !== sanPham.id);
        this.sanPham = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
