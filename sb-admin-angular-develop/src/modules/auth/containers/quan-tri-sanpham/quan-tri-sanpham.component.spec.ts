import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriSanphamComponent } from './quan-tri-sanpham.component';

describe('QuanTriSanphamComponent', () => {
  let component: QuanTriSanphamComponent;
  let fixture: ComponentFixture<QuanTriSanphamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriSanphamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriSanphamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
