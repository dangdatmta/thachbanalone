import { Route } from '@angular/compiler/src/core';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigReport } from '@app/models/ConfigReport';
import { AuthService } from '@modules/auth/services';
import { Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
declare var tableau: any;
@Component({
  selector: 'sb-embedded-iframe',
  templateUrl: './embedded-iframe.component.html',
  styleUrls: ['./embedded-iframe.component.scss']
})
export class EmbeddedIframeComponent implements OnInit, OnDestroy {
  public id!: number
  destroy$ = new Subject();
  vizUrl?: string;
  vizU?: string;
  viz: any;
  reinitialize = false;

  @ViewChild("vizContainer") containerDiv?: HTMLElement;
  key?: string;

  constructor(private serverHttp: AuthService, private activatedRoute: ActivatedRoute, private cdRef: ChangeDetectorRef) { }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        takeUntil(this.destroy$),
        map(params => {
          this.id = Number(params.get("id"));
          return this.id;
        }),
        switchMap(() => this.serverHttp.getConfigReports()),
        map((resp: { link: string; tableurl_id: string }[]) => {
          return resp.find(x => x.link === `/auth/Embed/${this.id}`);
        })
      )
      .subscribe((resp) => {
        this.serverHttp.getKeyTrust().subscribe((data) => {
          this.reinitialize = true;
          this.vizU = "http://192.168.62.113/trusted/" + data + resp?.tableurl_id.replace("http://tbi-tableau.thachban.vn", "") + "?:embed=y";
          this.vizUrl = this.vizU?.replace("showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link?:", "");
          // this.vizUrl = resp?.tableurl_id || this.vizUrl;
          // this.cdRef.detectChanges();
          // this.reinitialize = true;
          // this.cdRef.detectChanges();
        });



      });
  }

  getKey() {
    this.serverHttp.getKeyTrust().subscribe((data) => {
      this.key = data as string;
      console.log('key', this.key);
    });
  }
  initTableau() {
    const options = {
      hideTabs: false,
      // height: '900px',
      width: '1000',
      onFirstInteractive: () => {
      },
      onFirstVizSizeKnown: () => {
      }
    };
    var div = document.createElement("div") as Node;
    (this.containerDiv?.childNodes || []).forEach((child) => child.remove());
    this.containerDiv?.appendChild(div);

    this.viz = new tableau.Viz(
      div,
      this.vizUrl,
      options
    );
  }
}

// import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// import { ConfigReport } from '@app/models/ConfigReport';
// import { AuthService } from '@modules/auth/services';
// declare var tableau: any;
// @Component({
//   selector: 'sb-embedded-iframe',
//   templateUrl: './embedded-iframe.component.html',
//   styleUrls: ['./embedded-iframe.component.scss']
// })
// export class EmbeddedIframeComponent implements OnInit {

//   constructor(private serverHttp: AuthService,) { }
//   viz: any;
//   @ViewChild("vizContainer") containerDiv?: ElementRef;

//   ngOnInit() {
//     this.initTableau();
//   }

//   initTableau() {
//      //const containerDiv = document.getElementById("vizContainer");
//     this.serverHttp.getConfigReports().subscribe((data) => {
//       console.log('chbc', data)
//       this.serverHttp.getKeyTrust().subscribe((d2) => {

//         const vizUrl = "http://192.168.62.113/trusted/" + d2 +  data[data.length-1].tableurl_id.replace("http://tbi-tableau.thachban.vn","") + "?:embed=y";
//         console.log('link',vizUrl)
//       const options = {
//         hideTabs: false,
//         onFirstInteractive: () => {
//           console.log("onFirstInteractive");
//         },
//         onFirstVizSizeKnown: () => {
//           console.log("onFirstVizSizeKnown");
//         }
//       };
//       this.viz = new tableau.Viz(
//         this.containerDiv?.nativeElement,
//         vizUrl,
//         options
//       );
//       });

//     });

//   }
// }

