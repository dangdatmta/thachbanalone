import { Component, NgModule, OnInit } from '@angular/core';

import { Color } from '@app/models/Color';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-mausac',
  templateUrl: './quan-tri-mausac.component.html',
  styleUrls: ['./quan-tri-mausac.component.scss']
})
export class QuanTriMausacComponent implements OnInit {


  Color: Color[] = []
  colorDetail: string[] = []
  color?: Color
  EditDialog?: boolean
  colorSelected?: boolean
  productDialog?: boolean
  submitted?: boolean
  text?: string

  results?: string[];



  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }

  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào màu sắc' }];
    this.getAllColor()
    this.checkChucNang()
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | MS")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | MS")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | MS")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if (this.serverHttp.getTenChucNang().includes("Thêm | MS")||this.serverHttp.getTenChucNang().includes("Sửa | MS")||this.serverHttp.getTenChucNang().includes("Xóa | MS")) {
      this.checkXem = true
    } else {
      this.checkXem = false
    }
  }

  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  getAllColor() {
    this.serverHttp.getColor().subscribe((data) => {
      // console.log('data', data);
      this.Color = data;
    });
  }
  getColorId(id) {
    this.EditDialog = true;
    this.serverHttp.getColorID(id).subscribe((data) => {
      // console.log('data', data);
    });
  }
  openNew() {
    this.color = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(color: Color) {
    this.color = { ...color };
    this.EditDialog = true;
  }

  save() {
    this.submitted = false;
    if (this.color?.id) {
      this.Color[this.findIndexById(this.color.id)] = this.color;
      this.serverHttp.updateColor(this.color.id, this.color).subscribe((data) => { }
      );
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.color = {};
      //this.getAllRole()
    }
    else {
      //this.product.id = this.createId();
      this.serverHttp.addColor(this.color as Color).subscribe((data) => {});
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Thêm mới thành công', life: 3000 });
      this.color = {};
      this.getAllColor()
    }
    this.Color = [...this.Color];
    this.EditDialog = false;

  }
  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.Color.length; i++) {
      if (this.Color[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(color: Color) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + color.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteColor(color.id as any).subscribe((data) => {}
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.Color = this.Color.filter(val => val.id !== color.id);
        this.color = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
