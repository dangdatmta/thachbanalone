import { Component, OnInit } from '@angular/core';
import { NLDL_KyBC } from '@app/models/NLDL_KyBC';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-dong-mo-ky',
  templateUrl: './dong-mo-ky.component.html',
  styleUrls: ['./dong-mo-ky.component.scss'],
  providers: [MessageService, ConfirmationService]
})

export class DongMoKyComponent implements OnInit {
  editDialog?: boolean;
  submitted?: boolean;
  NLDL_KyBCs: NLDL_KyBC[] = []
  nldl_kybc?: NLDL_KyBC
  constructor(
    private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService
  ) { }
  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền đóng/mở kỳ' }];
    this.getNLDL_KyBCs()
    this.checkChucNang()
  }
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Đóng/Mở | KBC")) {
      this.checkXem = true
    } else {
      this.checkXem = false
    }
  }
  getNLDL_KyBCs() {
    this.serverHttp.getNLDL_KyBC().subscribe((data) => {
      this.NLDL_KyBCs = data;
    });
  }
  edit(nldl_kybc: NLDL_KyBC) {
    this.nldl_kybc = { ...nldl_kybc };
    this.editDialog = true;
  }
  openNew() {
    this.nldl_kybc = {};
    this.submitted = false;
    this.editDialog = true;
  }
  hideDialog() {
    this.editDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    if (this.nldl_kybc?.id) {
      this.NLDL_KyBCs[this.findIndexById(this.nldl_kybc.id)] = this.nldl_kybc;
      this.serverHttp.updateNLDL_KyBC(this.nldl_kybc.id).subscribe((data) => { }
      );
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Thay đổi trạng thái kỳ thành công', life: 3000 });
      this.nldl_kybc = {};
      this.NLDL_KyBCs = [...this.NLDL_KyBCs];
      this.editDialog = false;
    }
  }

  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.NLDL_KyBCs.length; i++) {
      if (this.NLDL_KyBCs[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }
}


