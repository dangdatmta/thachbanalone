import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DongMoKyComponent } from './dong-mo-ky.component';

describe('DongMoKyComponent', () => {
  let component: DongMoKyComponent;
  let fixture: ComponentFixture<DongMoKyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DongMoKyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DongMoKyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
