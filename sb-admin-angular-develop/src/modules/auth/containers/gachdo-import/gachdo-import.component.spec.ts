import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GachdoImportComponent } from './gachdo-import.component';

describe('GachdoImportComponent', () => {
  let component: GachdoImportComponent;
  let fixture: ComponentFixture<GachdoImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GachdoImportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GachdoImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
