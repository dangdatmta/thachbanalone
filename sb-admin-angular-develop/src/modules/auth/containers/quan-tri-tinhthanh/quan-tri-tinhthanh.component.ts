import { AfterViewInit, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TinhThanh } from '@app/models/TinhThanh';
import { AuthService } from '@modules/auth/services';
import { Message } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-tinhthanh',
  templateUrl: './quan-tri-tinhthanh.component.html',
  styleUrls: ['./quan-tri-tinhthanh.component.scss'],
})
export class QuanTriTinhthanhComponent implements OnInit {
  searchValue?: string
  EditDialog?: boolean
  submitted?: boolean
  id?: string
  TinhThanhs?: TinhThanh[] = []
  tinhthanh?: TinhThanh
  statuses?: any[];
  constructor(
    public sanitizer: DomSanitizer,
    private serverHttp: AuthService
  ) { this.sanitizer = sanitizer; }

  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{severity:'warn', summary:'Cảnh báo', detail:'Bạn không có quyền truy cập vào tỉnh thành'}];
    this.loadAllTinhThanh();
    this.checkChucNang();
  }
  checkchucnang!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Xem | TT")) {
      this.checkchucnang = true
    }
    else {
      this.checkchucnang = false
    }
  }
  loadAllTinhThanh() {
    this.serverHttp.getTinhThanhs().subscribe((data) => {
      this.TinhThanhs = data;
    });
  }
  edit(tinhthanh: TinhThanh) {
    this.tinhthanh = { ...tinhthanh };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
}

