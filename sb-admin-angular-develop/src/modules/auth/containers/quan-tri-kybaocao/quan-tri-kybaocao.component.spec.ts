import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriKybaocaoComponent } from './quan-tri-kybaocao.component';

describe('QuanTriKybaocaoComponent', () => {
  let component: QuanTriKybaocaoComponent;
  let fixture: ComponentFixture<QuanTriKybaocaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriKybaocaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriKybaocaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
