import { DOCUMENT } from '@angular/common';
import { Component, Inject, NgModule, OnInit } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { KyBaoCao } from '@app/models/KyBaoCao';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { UserRole } from '@app/models/UserRole';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-kybaocao',
  templateUrl: './quan-tri-kybaocao.component.html',
  styleUrls: ['./quan-tri-kybaocao.component.scss']
})
export class QuanTriKybaocaoComponent implements OnInit {
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean
  EditDialog1?: boolean
  submitted1?: boolean

  Periods: KyBaoCao[] = []
  period?: KyBaoCao
  year?: number
  NLDLs?: NhomLoaiDuLieu[]
  Period2: KyBaoCao[] = []

  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    @Inject(DOCUMENT) private _document: Document) { }
    msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào kỳ báo cáo' }];
    this.getAll()
    this.getAllNLDLs()
    this.getYears()
    this.checkChucNang()
  }
  checkXem!: boolean;
  checkchucnangThem!: boolean;
  checkchucnangdongmoky!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | KBC")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Đóng/Mở | KBC")) {
      this.checkchucnangdongmoky = true
    }
    else {
      this.checkchucnangdongmoky = false
    }
    if(this.serverHttp.getTenChucNang().includes("Đóng/Mở | KBC") || this.serverHttp.getTenChucNang().includes("Thêm | KBC")){
      this.checkXem = true
    }else{
      this.checkXem = false
    }
  }
  refreshPage() {
    this._document.defaultView?.location.reload();
  }
  nldl_id?: number;
  getKyFlowNLDL(nldl_id) {
    this.serverHttp.getKyFollowNLDL_id(nldl_id).subscribe((data) => {
      this.Period2 = data;
    });
  }
  Year?: number[]
  getYears() {
    this.serverHttp.getYears().subscribe((data) => {
      this.Year = data
    });
  }
  TrangThai?: number
  kybc_id?: number
  getTrangThai(nldl_id: number, kybc_id) {
    this.serverHttp.getTrangThai(nldl_id, kybc_id).subscribe((data) => {
      this.TrangThai = data
    });
  }
  KyBC?: KyBaoCao[]
  getLoaiKys(year, nldl_id) {
    this.serverHttp.getLoaiKys(year, nldl_id).subscribe((data) => {
      this.KyBC = data
      // console.log('year', year)
      // console.log('nldl_id', nldl_id)
    });
    this.KyBC = []
  }
  DongMoKy(nldl_id, kybc_id) {
    this.serverHttp.DongMoKy(nldl_id, kybc_id).subscribe((data) => {
    });
  }
  getAllNLDLs() {
    this.serverHttp.getNLDL().subscribe((data) => {
      this.NLDLs = data;
    });
  }
  getAll() {
    this.serverHttp.getPeriods().subscribe((data) => {
      // console.log('kybc', data)
      this.Periods = data;
    });
  }
  openNew() {
    this.period = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  openNew1() {
    this.submitted1 = false;
    this.EditDialog1 = true;
  }
  edit(period: KyBaoCao) {
    this.period = { ...period };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  hideDialog1() {
    this.EditDialog1 = false;
    this.submitted1 = false;
  }
  save() {
    this.submitted = true;
    if (this.period?.id) {
      this.Periods[this.findIndexById(this.period.id)] = this.period;
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.serverHttp.updatePeriod(this.period.id, this.period).subscribe((data) => { }
      );
    }
    this.Periods = [...this.Periods];
    this.period = {}
    this.EditDialog = false;
    this.refreshPage();
  }

  save1(nldl_id: number, kybc_id: number) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn thay đổi trạng thái kỳ này?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.submitted1 = true;
        this.serverHttp.DongMoKy(nldl_id, kybc_id).subscribe((data) => {
          this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });

          this.EditDialog = false;
        }
        );
      }
    })
  }

  add() {
    this.serverHttp.addPeriod(this.year as number).subscribe((data) => { });
    this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Tạo thành công', life: 3000 });
    this.Periods = [...this.Periods];
    this.period = {}
    this.getAll()
  }

  findIndexById(id?: number): number {
    let index = -1;
    for (let i = 0; i < this.Periods.length; i++) {
      if (this.Periods[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(period: KyBaoCao) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa kỳ báo cáo này?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deletePeriod(period.id as number).subscribe((data) => { }
        );
        this.Periods = this.Periods.filter(val => val.id !== period.id);
        this.period = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
