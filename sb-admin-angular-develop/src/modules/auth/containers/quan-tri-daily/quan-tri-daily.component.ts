import { Component, NgModule, OnInit } from '@angular/core';
import { TinhThanh } from '@app/models/TinhThanh';

import { DaiLy } from '@app/models/DaiLy';
import { AuthService } from '@modules/auth/services';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-daily',
  templateUrl: './quan-tri-daily.component.html',
  styleUrls: ['./quan-tri-daily.component.scss']
})
export class QuanTriDailyComponent implements OnInit {
  DaiLy: DaiLy[] = []
  TinhThanh: any[] = []
  daiLyDetail: string[] = []
  daiLy?: DaiLy
  EditDialog?: boolean
  daiLySelected?: boolean
  productDialog?: boolean
  submitted?: boolean
  text?: string
  selectedDaiLyAdvanced: any[] = []
  filteredDaiLy: any[] = []
  results?: string[];



  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }

  msg!: Message[];
  ngOnInit(): void {
    this.msg = [{ severity: 'warn', summary: 'Cảnh báo', detail: 'Bạn không có quyền truy cập vào đại lý' }];
    this.getAllDaiLy()
    this.getAllTinhThanh()
    this.checkChucNang()
  }
  checkchucnangThem!: boolean;
  checkchucnangSua!: boolean;
  checkchucnangXoa!: boolean;
  checkXem!: boolean;
  checkChucNang() {
    if (this.serverHttp.getTenChucNang().includes("Thêm | DL")) {
      this.checkchucnangThem = true
    }
    else {
      this.checkchucnangThem = false
    }
    if (this.serverHttp.getTenChucNang().includes("Sửa | DL")) {
      this.checkchucnangSua = true
    }
    else {
      this.checkchucnangSua = false
    }
    if (this.serverHttp.getTenChucNang().includes("Xóa | DL")) {
      this.checkchucnangXoa = true
    }
    else {
      this.checkchucnangXoa = false
    }
    if(this.serverHttp.getTenChucNang().includes("Thêm | DL") || this.serverHttp.getTenChucNang().includes("Sửa | DL") || this.serverHttp.getTenChucNang().includes("Xóa | DL")){
      this.checkXem = true
    }else{
      this.checkXem = false
    }
  }

  filterDaiLy(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = []
    let query = event.query;

    for (let i = 0; i < this.TinhThanh.length; i++) {
      let x = this.TinhThanh[i];
      if (x.ten.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(x);
      }
    }

    this.filteredDaiLy = filtered;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  getAllDaiLy() {
    this.serverHttp.getDaiLy().subscribe((data) => {
      // console.log('daily', data);
      this.DaiLy = data;
    });
  }
  getAllTinhThanh() {
    this.serverHttp.getTinhThanhs().subscribe((data) => {
      // console.log('tinhThanh', data);
      this.TinhThanh = data;
    });
  }
  getDaiLyId(id) {
    this.EditDialog = true;
    this.serverHttp.getDaiLyID(id).subscribe((data) => {
      // console.log('data', data);
    });
  }
  openNew() {
    this.daiLy = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(daiLy: DaiLy) {
    this.daiLy = { ...daiLy };
    this.EditDialog = true;
  }

  save() {
    this.submitted = false;
    if (this.daiLy?.id) {
      this.DaiLy[this.findIndexById(this.daiLy.id)] = this.daiLy;
      this.serverHttp.updateDaiLy(this.daiLy.id, this.daiLy).subscribe((data) => { }
      );
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.daiLy = {};
      //this.getAllRole()
    }
    else {
      //this.product.id = this.createId();
      this.serverHttp.addDaiLy(this.daiLy as DaiLy).subscribe((data) => { });
      this.messageService.add({ key: 'tr', severity: 'success', summary: 'Thành công', detail: 'Thêm mới thành công', life: 3000 });
      this.daiLy = {};
      this.getAllDaiLy()
    }
    this.DaiLy = [...this.DaiLy];
    this.EditDialog = false;

  }
  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.DaiLy.length; i++) {
      if (this.DaiLy[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(daiLy: DaiLy) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + daiLy.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteDaiLy(daiLy.id as any).subscribe((data) => { }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.DaiLy = this.DaiLy.filter(val => val.id !== daiLy.id);
        this.daiLy = {};
        this.messageService.add({ key: "tr", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
