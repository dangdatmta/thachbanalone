import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriDailyComponent } from './quan-tri-daily.component';

describe('QuanTriDailyComponent', () => {
  let component: QuanTriDailyComponent;
  let fixture: ComponentFixture<QuanTriDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriDailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
