/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/auth.guard';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AuthModule } from './auth.module';

/* Containers */
import * as authContainers from './containers';

/* Guards */
import * as authGuards from './guards';
import { AuthService } from './services';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login',
    },
    {
        path: 'login',
        canActivate: [],
        component: authContainers.LoginComponent,
        data: {
            title: 'Đăng nhập',
        } as SBRouteData,
    },
    {
        path: 'register',
        canActivate: [AuthGuard],
        component: authContainers.RegisterComponent,
        data: {
            title: 'Pages Register - SB Admin Angular',
        } as SBRouteData,
    },
    {
        path: 'forgot-password',
        canActivate: [AuthGuard],
        component: authContainers.ForgotPasswordComponent,
        data: {
            title: 'Pages Forgot Password - SB Admin Angular',
        } as SBRouteData,
    },
    {
        path: 'khuvuc',
        canActivate: [AuthGuard],
        component: authContainers.RegionComponent,
        data: {
            title: 'Khu vực',
        } as SBRouteData,
    },
    {
        path: 'tinhthanh',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriTinhthanhComponent,
        data: {
            title: 'Tỉnh thành',
        } as SBRouteData,
    },
    {
        path: 'quan-tri-nguoi-dung',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriNguoiDungComponent,
        data: {
            title: 'Thêm và phân quyền người dùng',
        } as SBRouteData,
    },
    {
        path: 'quan-tri-nhom-quyen',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriNhomQuyenComponent,
        data: {
            title: 'Quản trị nhóm quyền',
        } as SBRouteData,
    },
    // {
    //     path: 'quan-tri-nhom-quyen',
    //     canActivate: [],
    //     component: authContainers.QuanTriNhomQuyenComponent,
    //     data: {
    //         title: 'Thêm và phân quyền người dùng',
    //     } as SBRouteData,
    // },
    {
        path: 'quan-tri-phan-quyen',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriPhanQuyenComponent,
        data: {
            title: 'Thêm và phân quyền người dùng',
        } as SBRouteData,
    },
    {
        path: 'Embed/:id',
        canActivate: [AuthGuard],
        component: authContainers.EmbeddedIframeComponent,
        data: {
            title: 'Embed',
        } as SBRouteData,
    },
    {
        path: 'CSKH',
        canActivate: [AuthGuard],
        component: authContainers.StaticComponent,
        data: {
            title: 'Tài chính kế toán',
        } as SBRouteData,
    },
    {
        path: 'KD01CS',
        canActivate: [AuthGuard],
        component: authContainers.Kd01csComponent,
        data: {
            title: 'Kinh doanh CS',
        } as SBRouteData,
    },
    {
        path: 'nhomloaidulieu',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriNhomLoaiDuLieuComponent,
        data: {
            title: 'Nhóm loại dữ liệu',
        } as SBRouteData,
    },
    {
        path: 'configreport',
        canActivate: [AuthGuard],
        component: authContainers.ConfigReportComponent,
        data: {
            title: 'Cấu hình báo cáo',
        } as SBRouteData,
    },
    {
        path: 'kybaocao',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriKybaocaoComponent,
        data: {
            title: 'Kỳ báo cáo',
        } as SBRouteData,
    },
    {
        path: 'bophan',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriBophanComponent,
        data: {
            title: 'Bộ phận',
        } as SBRouteData,
    },
    {
        path: 'daily',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriDailyComponent,
        data: {
            title: 'Đại lý',
        } as SBRouteData,
    },
    {
        path: 'sanpham',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriSanphamComponent,
        data: {
            title: 'Sản phẩm',
        } as SBRouteData,
    },
    {
        path: 'mausac',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriMausacComponent,
        data: {
            title: 'Màu sắc',
        } as SBRouteData,
    },
    {
        path: 'nhomnguoisudung',
        canActivate: [AuthGuard],
        component: authContainers.QuanTriNhomnguoisudungComponent,
        data: {
            title: 'Nhóm người sử dụng',
        } as SBRouteData,
    },
    {
        path: 'gachdoreport',
        canActivate: [AuthGuard],
        component: authContainers.GachdoImportComponent,
        data: {
            title: 'Gạch đỏ Import',
        } as SBRouteData,
    },
    {
        path: 'dongmoky',
        canActivate: [AuthGuard],
        component: authContainers.DongMoKyComponent,
        data: {
            title: 'Đóng mở kỳ',
        } as SBRouteData,
    },
    {
        path: 'importdata',
        canActivate: [AuthGuard],
        component: authContainers.BaseUploadDuLieuComponent,
        data: {
            title: 'Upload dữ liệu',
        } as SBRouteData,
    },
];

@NgModule({
    imports: [AuthModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
